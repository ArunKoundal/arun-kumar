//
//  Utility.swift
//  ComunityCards
//
//  Created by Arun Kumar on 27/04/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class Utility: NSObject {

    class func showAlertMessage(title:String,message:String) {
        
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Ok", style: .cancel) { action -> Void in
            
        }
        alertController.addAction(cancelAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    class func isValidEmail(emailStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailStr)
    }
        
    
}

//MARK: UITextField Extension

extension UITextField {
    
    func setLeftIcon(_ image: UIImage) {
       let iconView = UIImageView(frame:
                      CGRect(x: 2, y: 2, width: 22, height: 22))
       iconView.image = image
        iconView.contentMode = .scaleAspectFit
       let iconContainerView: UIView = UIView(frame:
                      CGRect(x: 2, y: 0, width: 38, height: 30))
       iconContainerView.addSubview(iconView)
       leftView = iconContainerView
       leftViewMode = .always
    }
}

@IBDesignable
extension UITextField {

    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }

    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}

//MARK: UIDate Extension

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

//MARK: UIButton Extension

@IBDesignable extension UIButton {

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    func setUpCreditButtonView() {
        
        self.backgroundColor = UIColor.hexStringToUIColor(hex: "#E6FDE7")
        self.layer.borderColor = UIColor.hexStringToUIColor(hex: "#89CF8A").cgColor
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
    func setUpDebitButtonView() {
        
        self.backgroundColor = UIColor.hexStringToUIColor(hex: "#FBE5E5")
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
     func setUpButtonBackgroundColor() {
        
        self.backgroundColor = UIColor.hexStringToUIColor(hex: "#ECEBEB")
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 0
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
}

//MARK: UIView Extension

@IBDesignable extension UIView {

    @IBInspectable var viewBorderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var viewCornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var viewBorderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
     func setUpViewBorderColorGrey() {
        
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
    }
     func setUpViewBorderColorDefaultGreen() {
        
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.DefaultAppColor).cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
    }
}

//MARK: Color Extenstion

extension UIColor {
    
    class func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}

//MARK: Date Extenstion

extension NSDate {
    
    class func getDateStringFromDate (format:String,date:Date) -> String {
        
        var dateString:String
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateString = dateFormatter.string(from: date)
        
        if Constants.DEBUG.print { print("Date string is \(dateString)") }
        
        return dateString
        
    }
    
    class func getTimeStringFromDate () -> String {
        
        var timeString:String
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.dateFormat.TIME_FORMAT
        timeString = dateFormatter.string(from: Date())
        
        if Constants.DEBUG.print {print("Time string is \(timeString)")}
        
        return timeString
        
    }
    
    class func getDateFromString (format:String,dateString:String) -> Date {
        
        var date:Date
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        date = dateFormatter.date(from: dateString)!
        
        if Constants.DEBUG.print {print("Date is \(date)")}
        
        return date
    }
    
}

extension String {

    func heightWithConstrainedWidth(text:String,width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }

    func widthwithConstrainedHeight(text:String,height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }

    func trim() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
}
