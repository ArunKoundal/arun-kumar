//
//  ConfigurationTableFooterView.swift
//  ComunityCards
//
//  Created by Bunty on 05/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class ConfigurationTableFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var addDebitCreditImgView : UIImageView!
    @IBOutlet weak var netBankingImgView : UIImageView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    //MARK: Actions
    
    @IBAction func addDebitCreditCard(sender:UIButton) {
        
        
    }
    
    @IBAction func netBankingPayment(sender:UIButton) {
        
        
    }

}
