//
//  ConfigurationTableCell.swift
//  ComunityCards
//
//  Created by Bunty on 05/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class ConfigurationTableCell: UITableViewCell {
    
    @IBOutlet weak var carNumberLabel: UILabel!
    @IBOutlet weak var carHolderNameLabel: UILabel!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var radioImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Configure Add Amount Cell
    
    func configureConfigureCustomCell() {
        
    }

}
