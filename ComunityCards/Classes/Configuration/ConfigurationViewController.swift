//
//  ConfigurationViewController.swift
//  ComunityCards
//
//  Created by Arun Kumar on 28/01/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD
import Stripe

class ConfigurationViewController: UIViewController {

    var headerIdentifier = "ConfigurationHeaderView"
    var footerIdentifier = "ConfigurationFooterView"
    var paymentHeaderIdentifier = "PaymentTitleHeader"
    
    @IBOutlet weak var tblViewConfiguration: UITableView!

    @IBOutlet weak var backButton: UIBarButtonItem!

    @IBOutlet weak var checkTransHistoryBtn: UIButton!
    @IBOutlet weak var confirmPaymentBtn: UIButton!
    var totalAmount: String = ""
    var addMoneyAmount: String = ""

    var screenType : Bool = false
    var messageStatus : Bool = false
    var cardNumber : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpTableView()
        self.addOberver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.getUserTotalAmount()
        self.setUpNavigationItemViews()
    }
    
    //MARK: Set Up View
    func setUpNavigationItemViews() {
        
        // Get Screen Type
        if  UserDefaults.standard.value(forKey: "CREATEALERT") != nil {
            self.screenType = UserDefaults.standard.value(forKey: "CREATEALERT") as! Bool
        }
        if self.screenType == false {
            self.navigationItem.leftBarButtonItem = nil
            self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.clear
            self.navigationItem.title = "Add Money"
        }else {
            self.navigationItem.title = "Create Payment"
            if self.messageStatus == false {
                self.messageStatus = true
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Message!", message: "Please confirm payment to create alert")
                }
            }
        }

    }
    
    //MARK: Set up Table Header
    
    func setUpTableView() {

        let headerNib = UINib(nibName: "ConfigurationTableHeaderView", bundle: nil)
        self.tblViewConfiguration.register(headerNib, forHeaderFooterViewReuseIdentifier: headerIdentifier)
//        let paymentHeaderNib = UINib(nibName: "PamentTitleHeaderView", bundle: nil)
//        self.tblViewConfiguration.register(paymentHeaderNib, forHeaderFooterViewReuseIdentifier: paymentHeaderIdentifier)
//        let footerNib = UINib(nibName: "ConfigurationTableFooterView", bundle: nil)
//        self.tblViewConfiguration.register(footerNib, forHeaderFooterViewReuseIdentifier: footerIdentifier)
        
    }
    
    //MARK: Add Oberver
    func addOberver() {
        
        // Register to receive token
        NotificationCenter.default.addObserver(self, selector: #selector(ConfigurationViewController.getTokenInfo), name: NSNotification.Name(rawValue: "GetPaymentToken"), object: nil)
        
    }
    // Get Stripe Token
    @objc func getTokenInfo(withNotification notification: NSNotification) {
        
        let tokenInfo = notification.userInfo as! Dictionary<String,Any>
        if Constants.DEBUG.print { print("Info is \(tokenInfo)") }

        if tokenInfo["Token"] != nil {
            
            //let token = tokenInfo["Token"] as! STPToken
            self.createPaymentCall()
        }else {
            DispatchQueue.main.async {
                Utility.showAlertMessage(title: "Alert!", message: "Something went wrong, please try again")
            }
        }
        
    }
    
    //MARK: Get User Total Amount
    func getUserTotalAmount() {
        
        SVProgressHUD.show()
        CommunityAPI.getUserTotalAmount { (response, error) in
            SVProgressHUD.dismiss()
            if response.count > 0 {
                self.totalAmount = response
                DispatchQueue.main.async {
                    self.tblViewConfiguration.reloadData()
                }
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                }
            }
        }
        
    }
    
    //MARK: Create Payment Call
    func createPaymentCall() {
        let postData = self.getCreatePaymentParamsInfo()
        SVProgressHUD.show()
        CommunityAPI.createPayment(postData: postData) { (response, error) in
            SVProgressHUD.dismiss()
            if response.count > 0 {
                if Constants.DEBUG.print { print("Response is \(response)") }
                UserDefaults.standard.set(false, forKey: "CREATEALERT")
                UserDefaults.standard.synchronize()
                self.addMoneyAmount = ""
                DispatchQueue.main.async {
                    guard let paymentConfirmController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentConfirmController") else { return }
                    self.navigationController?.pushViewController(paymentConfirmController, animated: true)
                }
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                }
            }

        }
    }
    
    //MARK: Get Payment Info Parameters
    
    func getCreatePaymentParamsInfo() -> Data {
        
        var userID = ""
        if let userId = UserDefaults.standard.value(forKey: Constants.STRINGS.USERID) {
            userID = userId as! String
        }
        var operation = "LOAD"
        var alertId = "-1"
        if self.screenType == true {
            operation = "ALERT"
            alertId = "1"
        }
        if self.addMoneyAmount == "" {
            self.addMoneyAmount = "50"
        }
        let parameters = "userid=\(userID)&amount=\(self.addMoneyAmount)&operation=\(operation)&alertid=\(alertId)"
        let postData =  parameters.data(using: .utf8)
        
        return postData!
    }
    
    //MARK: Check Transaction History Acton
    
    @IBAction func checkTransactionHistoryAction(_ sender:Any) {
        
        let transactionController = self.storyboard?.instantiateViewController(withIdentifier: "TransactionHistoryController") as! TransactionHistoryViewController
        transactionController.totalAmount = self.totalAmount
        self.navigationController?.pushViewController(transactionController, animated: true)
        
    }
    
    //ConfirmPayment
    @IBAction func confirmPaymentAction(_ sender:Any) {
        
        if self.addMoneyAmount.count <= 0 {
            Utility.showAlertMessage(title: "", message: "Please enter amount")
            return
        }
        let stripeController = self.storyboard?.instantiateViewController(withIdentifier: "StripeViewController") as! StripeViewController
        stripeController.amount = Int(self.addMoneyAmount)!
        stripeController.modalPresentationStyle = .fullScreen
        self.present(stripeController, animated: true, completion: nil)
    }

    //Back Action
    @IBAction func backAction(_ sender:Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }

}

extension ConfigurationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
//        let height : CGFloat = indexPath.section == 0 ? 0 : 90

        return 0 //UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //let number : Int = section == 0 ? 0 : 2

        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
//        if indexPath.section == 1 {
//            cell = tableView.dequeueReusableCell(withIdentifier: "BankAccountDetail") as! ConfigurationTableCell
//            return cell
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        //let height : CGFloat = section == 0 ? 158 : 41

        return 158.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
//        var headerView = UITableViewHeaderFooterView()
//
//        if section == 0 {
        let headerView = self.tblViewConfiguration.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! ConfigurationTableHeaderView
        headerView.configureTableHeaderView(totalAmount: self.totalAmount)
        headerView.delegate = self
        return headerView
//        }
//        else {
//            headerView = self.tblViewConfiguration.dequeueReusableHeaderFooterView(withIdentifier: paymentHeaderIdentifier) as! PamentTitleHeaderView
//            return headerView
//        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
//        let height : CGFloat = section == 0 ? 0 : 103

        return 0
    }
    
/*    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        var footerView = UIView()
        
        if section == 1 {
            footerView = self.tblViewConfiguration.dequeueReusableHeaderFooterView(withIdentifier: footerIdentifier) as! ConfigurationTableFooterView
            return footerView
        }
        return footerView

    }
    */
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
}

extension ConfigurationViewController: AddMoneyDelegate {
    
    func getAddMoney(addMoney: String) {
        
        if Constants.DEBUG.print { print("Entered Amount is \(addMoney)") }
        self.addMoneyAmount = addMoney
    }
}
