//
//  CreditInfoTableCell.swift
//  ComunityCards
//
//  Created by Bunty on 09/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class CreditInfoTableCell: UITableViewCell {

    @IBOutlet weak var priceLable : UILabel!
    @IBOutlet weak var dateLable : UILabel!
    @IBOutlet weak var timeLable : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: Configure Credit Cell
    
    func configureCreditCell(transactionInfo:TransactionHistory) {
        
        self.timeLable.isHidden = true
        if let amountValue = transactionInfo.amount {
            self.priceLable.text = "$\(amountValue)"
        }
        if let transactionDate = transactionInfo.formattedCompletedDate {
            self.dateLable.text = transactionDate
        }
        if let transactionTime = transactionInfo.timeStr {
            self.timeLable.text = transactionTime
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
