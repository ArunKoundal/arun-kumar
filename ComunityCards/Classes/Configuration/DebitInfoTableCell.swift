//
//  DebitInfoTableCell.swift
//  ComunityCards
//
//  Created by Bunty on 09/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class DebitInfoTableCell: UITableViewCell {
    
    @IBOutlet weak var communityNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var debitLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureDebitHistoryCell(transactionInfo:TransactionHistory) {
        
        self.timeLable.isHidden = true

        if let amountValue = transactionInfo.amount {
            self.priceLabel.text = "$\(amountValue)"
        }
        if let transactionDate = transactionInfo.formattedCompletedDate {
            self.dateLabel.text = transactionDate
        }
        if let communityName = transactionInfo.collectionName {
            self.communityNameLabel.text = communityName
        }
        if let transactionTime = transactionInfo.timeStr {
            self.timeLable.text = transactionTime
        }
        if let cardNo = transactionInfo.cardNumber {
            self.cardNumberLabel.text = "\(cardNo)"
        }
    }
    
}
