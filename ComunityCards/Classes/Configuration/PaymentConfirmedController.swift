//
//  PaymentConfirmedController.swift
//  ComunityCards
//
//  Created by Bunty on 09/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class PaymentConfirmedController: UIViewController {
    
    @IBOutlet weak var backToHomeBtn : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }

    //MARK: Back TO Home
    
    @IBAction func backToHomeAction() {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popViewController(animated: false)

    }

}
