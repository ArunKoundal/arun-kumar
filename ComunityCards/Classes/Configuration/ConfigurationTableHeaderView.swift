//
//  ConfigurationTableHeaderView.swift
//  ComunityCards
//
//  Created by Bunty on 05/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

protocol AddMoneyDelegate : class {
    
    func getAddMoney(addMoney:String)
}

class ConfigurationTableHeaderView: UITableViewHeaderFooterView,UITextFieldDelegate {
    
    @IBOutlet weak var totalBalanceLabel : UILabel!
    @IBOutlet weak var enterAmountTextField : UITextField!
    weak var delegate: AddMoneyDelegate?
    
    // ConfigureHeaderView
    func configureTableHeaderView(totalAmount:String) {
        
        self.totalBalanceLabel.text = "$ \(totalAmount)"
        self.enterAmountTextField.text = "0"
        self.setUpToolBar()
    }
    
    // Set Up Tool Bar
    func setUpToolBar() {
        
        let toolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        toolbar.items = [
        UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)),
        UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
        UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneWithNumberPad))]
        toolbar.sizeToFit()
        self.enterAmountTextField.inputAccessoryView = toolbar
    }

    @objc func cancelNumberPad() {
        self.enterAmountTextField.resignFirstResponder()
    }
    @objc func doneWithNumberPad() {
        self.enterAmountTextField.resignFirstResponder()
        if self.enterAmountTextField.text!.count > 0 {
            self.delegate?.getAddMoney(addMoney: self.enterAmountTextField.text!)
        }
    }
    
    //MARK: TextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
