//
//  TransactionHistoryViewController.swift
//  ComunityCards
//
//  Created by Bunty on 04/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD

class TransactionHistoryViewController: UIViewController {
    
     let headerIdentifier = "TransactionHistoryHeader"
     let creditCellIdentifier = "CreditInfoTableCell"
     let debitCellIdentifier = "DebitInfoTableCell"
    
    @IBOutlet weak var totalAvailBalanceLbl: UILabel!
    @IBOutlet weak var creditButton: UIButton!
    @IBOutlet weak var debitButton: UIButton!
    @IBOutlet weak var transactionHistoryTblView: UITableView!
    var selectedIndex : Int = 0
    var creditTransactionArray : [TransactionHistory] = []
    var debitTransactionArray : [TransactionHistory] = []
    var totalAmount : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpTableView()
        self.setCreditButtonView()
        self.getTransactionHistory()
        self.totalAvailBalanceLbl.text = self.totalAmount
    }
    
    //MARK: Set up Table Header
    
    func setUpTableView() {

//        let headerNib = UINib(nibName: "TransactionHistoryHeader", bundle: .main)
//        self.transactionHistoryTblView.register(headerNib, forHeaderFooterViewReuseIdentifier: headerIdentifier)
        let creditCell = UINib(nibName: "CreditInfoTableCell", bundle: .main)
        self.transactionHistoryTblView.register(creditCell, forCellReuseIdentifier: creditCellIdentifier)
        let debitCell = UINib(nibName: "DebitInfoTableCell", bundle: .main)
        self.transactionHistoryTblView.register(debitCell, forCellReuseIdentifier: debitCellIdentifier)
    }
    
    
    func setCreditButtonView() {
        
        self.creditButton.setUpCreditButtonView()
        self.debitButton.setUpButtonBackgroundColor()
        self.selectedIndex = 0
    }
    
    //MARK: Credit Action
    
    @IBAction func creditAction(_ sender: UIButton) {
        
        self.setCreditButtonView()
        self.transactionHistoryTblView.reloadData()
    }
    
    //MARK: Debit Action
    
    @IBAction func debitAction(_ sender: UIButton) {
        
        self.debitButton.setUpDebitButtonView()
        self.creditButton.setUpButtonBackgroundColor()
        self.selectedIndex = 1
        self.transactionHistoryTblView.reloadData()

    }
    
    //MARK: Back Action
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Get Transaction History
    func getTransactionHistory() {
        SVProgressHUD.show()
        CommunityAPI.getTransactionHistory { (transactionArray, error) in
            SVProgressHUD.dismiss()
            
            if error.count > 0 {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                }
            }else {
                self.filterTransactionArray(transactionsArray: transactionArray!)
            }
        }
    }
    //Filter Transaction Array
    func filterTransactionArray(transactionsArray: [TransactionHistory]) {
        
        self.creditTransactionArray.removeAll()
        self.debitTransactionArray.removeAll()
        
        for transactionInfo in transactionsArray {
            
            if let transactionType = transactionInfo.transactionType {
                
                if transactionType == "LOAD" {
                    self.creditTransactionArray.append(transactionInfo)
                }else if transactionType == "ALERT"{
                    self.debitTransactionArray.append(transactionInfo)
                }
            }
        }
        DispatchQueue.main.async {
            self.transactionHistoryTblView.reloadData()
        }
    }
    
}

extension TransactionHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let height : CGFloat = self.selectedIndex == 0 ? 45 : 50

        return height //UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let number : Int = self.selectedIndex == 0 ? self.creditTransactionArray.count : self.debitTransactionArray.count

        return number
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if self.selectedIndex == 0 {
           let cell = tableView.dequeueReusableCell(withIdentifier: creditCellIdentifier) as! CreditInfoTableCell
            cell.configureCreditCell(transactionInfo:self.creditTransactionArray[indexPath.row])
            return cell
        }else {
           let cell = tableView.dequeueReusableCell(withIdentifier: debitCellIdentifier) as! DebitInfoTableCell
            cell.configureDebitHistoryCell(transactionInfo: self.debitTransactionArray[indexPath.row])
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.0 //45.0
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//          let headerView = self.transactionHistoryTblView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! TransactionHistoryHeader
//            return headerView
//
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.transactionHistoryTblView.deselectRow(at: indexPath, animated: true)
    }
    
}

