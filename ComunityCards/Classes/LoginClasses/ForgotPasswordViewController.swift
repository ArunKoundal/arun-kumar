//
//  ForgotPasswordViewController.swift
//  ComunityCards
//
//  Created by Bunty on 09/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var forgotPasswordUserNameTxtField: UITextField!
    @IBOutlet weak var sendButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.forgotPasswordUserNameTxtField.setLeftIcon(UIImage(named: "usernameIcon") ?? UIImage())

    }
    
    //MARK: Send Action Method

    @IBAction func sendEmailAction(_ sender: Any) {
        
        let status:Bool = self.checkForUserNameValidations()
        if status == true {
            self.view.endEditing(true)
            self.getVerificationCodeToChangePassword()
        }

    }
    
    //MARK: Get Verification Code
    func getVerificationCodeToChangePassword(){
        
        SVProgressHUD.show()
        CommunityAPI.getForgotPasswordVerificationCode(username: self.forgotPasswordUserNameTxtField.text!) { (response, error) in
            SVProgressHUD.dismiss()
            if response.count > 0 {
                DispatchQueue.main.async {
                    self.showAlertWithMessage()
                }
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                }
            }
        }
        
    }
    
    func showAlertWithMessage() {
        
        let alert = UIAlertController(title: "Message", message: "Verification Code Send to your registered email, Press OK To Proceed ", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            let resetPasswordC = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordController") as! ResetPasswordController
            self.navigationController?.pushViewController(resetPasswordC, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: Check For Validations
    
    func checkForUserNameValidations() -> Bool {
        
        if (self.forgotPasswordUserNameTxtField.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter user name")
            return false
        }
        return true
    }
    
    //MARK: Back Action Method
    
    @IBAction func backActionMethod(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }


}

//MARK: Text field Extention

extension ForgotPasswordViewController:  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
  }
    
}
