//
//  RegisterViewController.swift
//  ComunityCards
//
//  Created by Arun Kumar on 28/01/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import CoreLocation

class RegisterViewController: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var txtFieldEmail:UITextField!
    @IBOutlet weak var txtFieldPassword:UITextField!
    @IBOutlet weak var txtFieldConfirmPass:UITextField!
    @IBOutlet weak var txtFieldUsername:UITextField!

    var locationManager: CLLocationManager!
    var currentLat : CLLocationDegrees = 0.0
    var currentLong : CLLocationDegrees = 0.0
    
    let communityService = CommunityAPI()
    var screenType = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtFieldEmail.setLeftIcon(UIImage(named: "emailIcon") ?? UIImage())
        self.txtFieldPassword.setLeftIcon(UIImage(named: "passwordIcon") ?? UIImage())
        self.txtFieldConfirmPass.setLeftIcon(UIImage(named: "passwordIcon") ?? UIImage())
        self.txtFieldUsername.setLeftIcon(UIImage(named: "usernameIcon") ?? UIImage())
        
        self.configureLocationManager()
    }


    func openTabbarController() {
        
    }
    
    //MARK: Configure Location Manager
    
    func configureLocationManager() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 50
        
        // Check for Location Services
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    // MARK - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // User location
        if locations.last != nil {
            let location = locations.last
            self.currentLat = (location?.coordinate.latitude)!
            self.currentLong = (location?.coordinate.longitude)!
            locationManager.stopUpdatingLocation()
        }
    }
    
    //MARK: Register User

    @IBAction func action_registration(_ sender: Any) {
//        guard let tabBarCont = self.storyboard?.instantiateViewController(withIdentifier: "AppTabBarController") else { return }
//        self.navigationController?.pushViewController(tabBarCont, animated: true)
        
        let status:Bool = self.checkForRegisterValidations()
        if status == true {
            self.view.endEditing(true)
            self.registerUser()
        }
    }
    
    func registerUser() {
               
        if Constants.Connectivity.isConnectedToInternet {
            
            let postData = self.getRegisterParametersInfo()
            if Constants.DEBUG.print { print("Params Data is \(postData)") }
            let urlStr = BASEURL.URL + BASEURL.API.REGISTER
            if Constants.DEBUG.print { print("Url is \(urlStr)") }
            var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.httpBody = postData

            SVProgressHUD.show()

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
               SVProgressHUD.dismiss()
              guard let data = data else {
                if Constants.DEBUG.print { print(String(describing: error)) }
                return
              }
              let responseStr = String(data: data, encoding: .utf8)!
              if Constants.DEBUG.print { print("Response is \(responseStr)") }
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                        self.swichToLoginView()
                    }else {
                        DispatchQueue.main.async {
                            Utility.showAlertMessage(title: "Alert!", message: responseStr)
                        }
                    }
                }
            }
            task.resume()
        }else {
            DispatchQueue.main.async {
                Utility.showAlertMessage(title: "Alert!", message: "No Internet Connection Found")
            }
        }

    }
    
    //MARK: Switch Based on ScreenType
    
    func swichToLoginView() {
        
        if self.screenType == false {
            
            DispatchQueue.main.async {
                let loginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(loginController, animated: true)
                Utility.showAlertMessage(title: "Message!", message: "Registered Successfully. Please login")
            }
            
        }else {
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
                Utility.showAlertMessage(title: "Message!", message: "Registered Successfully. Please login")
            }
        }
        
    }
    
    func checkResponseStatusCode(response:HTTPURLResponse) -> Bool {
        
        if response.statusCode == 200 || response.statusCode == 201 {
            return true
        }else {
            Utility.showAlertMessage(title: "Alert!", message: "Something went wrong!")
            return false
        }
    }
    
    func parseRegisterInfoDict(json:Dictionary<String,Any>){
        
        if json["success"] != nil {
            
            let success = json["success"] as! Bool
            if success == true {
                
                //let result = json["result"] as! Dictionary<String,Any>

            }else {
                SVProgressHUD.dismiss()
                Utility.showAlertMessage(title: "Alert!", message: (json["errorMessage"] as! String))
            }
        }
        
    }

    //MARK: Get Register Parameters Info
    
    func getRegisterParametersInfo() -> Data {
        
        let emailId = self.txtFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = self.txtFieldPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let userName = self.txtFieldUsername.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let country = NSLocale.current.regionCode
        //let country = "IN"
        let reggpslat = self.currentLat
        let reggpslon = self.currentLong
                
        let parameters = "username=\(userName!)&password=\(password!)&email=\(emailId!)&country=\(country!)&reggpslat=\(reggpslat)&reggpslon=\(reggpslon)"
        
        if Constants.DEBUG.print { print("Parameters info is \(parameters)") }

        let registerData =  parameters.data(using: .utf8)

        return registerData!
        
    }
    
    //MARK: Check For Register Validations
    
    func checkForRegisterValidations() -> Bool {
        
        if (self.txtFieldEmail.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter email")
            return false
        }
        if (self.txtFieldEmail.text?.count)! > 0 && !(Utility.isValidEmail(emailStr: self.txtFieldEmail.text!)) {
            Utility.showAlertMessage(title: "", message: "Please enter valid email")
            return false
        }
        if (self.txtFieldPassword.text?.count)! < 8 {
            Utility.showAlertMessage(title: "", message: "Please enter password min 8 length")
            return false
        }
        if (self.txtFieldConfirmPass.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter password again")
            return false
        }
        if self.txtFieldPassword.text != self.txtFieldConfirmPass.text {
            Utility.showAlertMessage(title: "", message: "Password doesn't match")
            return false
        }
        if (self.txtFieldUsername.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter user name")
            return false
        }
        if self.currentLat == 0.0 {
            Utility.showAlertMessage(title: "", message: "Please allow location permission")
            return false
        }
        return true
    }
    
    //MARK: Switch to Register Screen
    
    @IBAction func switchToLoginScreen(_ sender: Any) {
    
         self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: Text field Extention

extension RegisterViewController:  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
  }
    
}
