//
//  ResetPasswordController.swift
//  ComunityCards
//
//  Created by Bunty on 17/06/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD


class ResetPasswordController: UIViewController {

    @IBOutlet weak var userNameTxtField: UITextField!
    @IBOutlet weak var tokenTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpViews()
        self.addToolBarOnKeyboard()
    }
    
    func setUpViews() {
        
        self.userNameTxtField.setLeftIcon(UIImage(named: "usernameIcon") ?? UIImage())
        self.tokenTxtField.setLeftIcon(UIImage(named: "Token-icon") ?? UIImage())
        self.passwordTxtField.setLeftIcon(UIImage(named: "passwordIcon") ?? UIImage())
    }
    
    //MARK: Reset Password Action
    @IBAction func resetDoneAction() {
        
        let status:Bool = self.checkForResetFiedlsValidations()
        if status == true {
            self.view.endEditing(true)
            self.resetPassword()
        }
    }
    
    //MARK: Reset Password
    func resetPassword() {
        
        SVProgressHUD.show()
        CommunityAPI.resetPasswordWithVerificationCode(username: self.userNameTxtField.text!, token: self.tokenTxtField.text!, password: self.passwordTxtField.text!) { (responseStr, error) in
            SVProgressHUD.dismiss()
            if responseStr.count > 0 {
                DispatchQueue.main.async {
                    self.showAlertWithMessage()
                }
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                }
            }
        }
        
    }
    
    func showAlertWithMessage() {
        
        let alert = UIAlertController(title: "Message", message: "Password reset successfully.", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Check For Register Validations
    
    func checkForResetFiedlsValidations() -> Bool {
        
        if (self.userNameTxtField.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter user name")
            return false
        }
        if (self.tokenTxtField.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter token")
            return false
        }
        if (self.passwordTxtField.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter password")
            return false
        }
        if (self.passwordTxtField.text?.count)! < 8 {
            Utility.showAlertMessage(title: "", message: "Please enter 8 characters password")
            return false
        }
        return true
    }

    //MARK: Back Action
    @IBAction func backActionMethod() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Add Toolbar On Keyboard
    func addToolBarOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.userNameTxtField.inputAccessoryView = doneToolbar;
        self.tokenTxtField.inputAccessoryView = doneToolbar;
        self.passwordTxtField.inputAccessoryView = doneToolbar;

    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }

}

extension ResetPasswordController:  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
  }
}
