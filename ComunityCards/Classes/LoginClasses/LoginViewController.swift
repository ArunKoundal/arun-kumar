//
//  LoginViewController.swift
//  ComunityCards
//
//  Created by Arun Kumar on 27/04/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class LoginViewController: UIViewController {

    @IBOutlet weak var txtFieldUserName:UITextField!
    @IBOutlet weak var txtFieldPassword:UITextField!
    @IBOutlet weak var lblUserNameTitle:UILabel!

    let communityService = CommunityAPI()
    var userName : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtFieldUserName.placeholder = "User Name"
        self.lblUserNameTitle.text = "User Name"
        self.txtFieldUserName.setLeftIcon(UIImage(named: "usernameIcon") ?? UIImage())
        self.txtFieldPassword.setLeftIcon(UIImage(named: "passwordIcon") ?? UIImage())
    }
    
    //MARK: Login Action Method
    
    @IBAction func action_login(_ sender: Any) {
        
//         guard let tabBarCont = self.storyboard?.instantiateViewController(withIdentifier: "AppTabBarController") else { return }
//         self.navigationController?.pushViewController(tabBarCont, animated: true)
        
        let status:Bool = self.checkForLoginValidations()
        if status == true {
            self.view.endEditing(true)
            self.loggedInUser()
        }
     }
    
    func loggedInUser() {
        
        if Constants.Connectivity.isConnectedToInternet {
            
            let postData =  self.getLoginParametersInfo()
            let urlStr = BASEURL.URL + BASEURL.API.LOGIN
            if Constants.DEBUG.print { print("Url is \(urlStr)") }
            var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.httpBody = postData
            
            SVProgressHUD.show()

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                
              guard let data = data else {
                 SVProgressHUD.dismiss(withDelay: 1)
                if Constants.DEBUG.print { print(String(describing: error)) }
                return
              }
              let tokenStr = String(data: data, encoding: .utf8)!
              if Constants.DEBUG.print { print("Token is \(tokenStr)") }
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                        self.getUserID(tokenStr: tokenStr)
                    }else {
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            Utility.showAlertMessage(title: "Alert!", message: tokenStr)
                        }
                    }
                }
            }
            task.resume()
        }else {
            Utility.showAlertMessage(title: "Alert!", message: "No Internet Connection Found")
        }

    }
    
    //MARK: Switch To View
    
    func getUserID(tokenStr:String) {
        SVProgressHUD.dismiss(withDelay: 1)
        UserDefaults.standard.set(tokenStr, forKey: Constants.STRINGS.TOKEN)
        UserDefaults.standard.set(self.userName, forKey: Constants.STRINGS.USERNAME)
        UserDefaults.standard.synchronize()
        self.getCustomerId()
        
    }
    
    //MARK: Get Customer ID
    
    func getCustomerId() {
        
        CommunityAPI.getUserID(userName: self.userName) { (userId, error) in
            //SVProgressHUD.dismiss()
            if userId.count > 0 {
                if Constants.DEBUG.print { print("User ID is \(userId)") }
                UserDefaults.standard.set(userId, forKey: Constants.STRINGS.USERID)
                UserDefaults.standard.synchronize()
                self.submitDeviceTokenToServer()
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: "\(error)")
                }
            }
            
        }
    }
    
    //MARK: Get Login Parameters Info
    
    func getLoginParametersInfo() -> Data {
        
        self.userName = (self.txtFieldUserName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        let password = self.txtFieldPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let loginParams = "username=\(userName)&password=\(password!)"
        if Constants.DEBUG.print { print("Parameters info is \(loginParams)") }

        let loginData =  loginParams.data(using: .utf8)

        return loginData!
        
    }
    
    //MARK: Check For Register Validations
    
    func checkForLoginValidations() -> Bool {
        
        if (self.txtFieldUserName.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter user name")
            return false
        }
        if (self.txtFieldPassword.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter password")
            return false
        }
        return true
    }
    
    //MARK: Switch to Register Screen
    
    @IBAction func switchToRegisterScreen(_ sender: Any) {
        
        let registerController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        registerController.screenType = true
         self.navigationController?.pushViewController(registerController, animated: true)
    }
    
    //MARK: Switch to Forgot Password Screen
    
    @IBAction func switchToForgotPasswordScreen(_ sender: Any) {
        
         guard let forgotPassController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordController") else { return }
         self.navigationController?.pushViewController(forgotPassController, animated: true)
    }
    
    //MARK: Submit Token to Server
    
    func submitDeviceTokenToServer() {
        
        if (UserDefaults.standard.value(forKey: Constants.STRINGS.DEVICE_TOKEN) as? String) != nil {
            
            CommunityAPI.submitDeviceTokenToServer { (response, error) in
                SVProgressHUD.dismiss()
                
                if response.count > 0 {
                    
                    self.makeTabBarAsRoot()
                    
                }else {
                    DispatchQueue.main.async {
                        Utility.showAlertMessage(title: "Alert!", message: "\(error)")
                    }
                }
                
            }
            
        }else {
            SVProgressHUD.dismiss()
            self.makeTabBarAsRoot()
        }
        
    }
    
    func makeTabBarAsRoot() {
        
        DispatchQueue.main.async {
            guard let tabBarCont = self.storyboard?.instantiateViewController(withIdentifier: "AppTabBarController") else { return }
            self.navigationController?.pushViewController(tabBarCont, animated: true)
        }
    }
    
    
}

extension LoginViewController:  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
  }
}

   /*
         
 /*      let loginData = self.getLoginParametersInfo()
         
         communityService.postLoginForm(postData: loginData) { result in
             
             switch result {
             case .failure(_ ):
                 break
                 
             case .success(let model):
                 if let data = model {
                     if Constants.DEBUG.print { print("Data is \(data)") }
                 }
                 break
             }
         }
  */
 
   let parameterDictionary = self.getLoginParametersInfoDict()
   if Constants.APPDEBUG.print { print("Parma info is \(parameterDictionary)") }
   
   let headers : HTTPHeaders = ["Content-Type": "application/x-www-form-urlencoded"]
   
   if Constants.Connectivity.isConnectedToInternet {
       
       SVProgressHUD.show()
       AF.request(urlString, method: .post, parameters: (parameterDictionary as Parameters), encoding: URLEncoding.default, headers: headers).response { response in
           SVProgressHUD.dismiss()
           
           switch response.result {
           case .success(let data):
               if Constants.APPDEBUG.print { print("Response code is : \(String(describing: response.response?.statusCode))") }
               if Constants.APPDEBUG.print { print("Data is : \(String(describing: data))") }

           case .failure(let error):
               if Constants.APPDEBUG.print { print(error) }
           }
       }
   }else {
       
       Utility.showAlertMessage(title: "Alert Message", message: "No Internet Connection Found")
   }*/
