//
//  CommunityClasses.swift
//  ComunityCards
//
//  Created by Bunty on 13/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import Foundation
import UIKit

//MARK: Community Collection
struct CommunityCollection {
    
    var collectionId : String?
    var collectionName : String?
    var collectionImage : String?
    
    init(data:[Any]) {
        
        if data.count >= 2 {
            collectionId = data[0] as? String
            collectionName = data[1] as? String
            if !(data[2] is NSNull) {
                collectionImage = data[2] as? String
            }
        }
    }

}

struct CollectionDetail {
    
    var cardNumber : Int?
    var nearAreaCardCount : Int?
    var peopleCountCard : Int?
    var noOfAlerts : Int?

    init(data:[Int]) {
        
        if data.count >= 3 {
            cardNumber = data[0]
            nearAreaCardCount = data[1]
            peopleCountCard = data[2]
            noOfAlerts = data[3]
        }
    }
    
}

struct CardDetail {
    
    var userName : String?
    var id : String?
    var distance : Double?
    var distanceInStr : String?
    
    init(dataInfo:Dictionary<String,Any>) {
        
        userName = dataInfo["username"] as? String
        id = dataInfo["id"] as? String
        distance = dataInfo["distance"] as? Double
        if let distanceValue = distance {
            //let distanceInKm = Double(distanceValue/1000)
            distanceInStr = String(format:"%.1f Km", distanceValue)
        }
    }

}

struct AlertDetail {
    
    var collectionName : String?
    var cardNumber : Int?
    var completedDateStr : String?
    var timeStr : String?
    var formattedCompletedDate : String?
    var date : Date!
    var alertId : String?

    init(data:[Any]) {
        
        if data.count >= 4 {
            collectionName = data[0] as? String
            cardNumber = data[1] as? Int
            if let dateStr = data[2] as? String {
                completedDateStr = dateStr
                self.date = NSDate.getDateFromString(format: Constants.dateFormat.DATE_FORMAT, dateString: completedDateStr!)
                formattedCompletedDate = NSDate.getDateStringFromDate(format: Constants.dateFormat.DATE_FORMAT_UI, date: self.date!)
            }
            if !(data[3] is NSNull) {
                timeStr = data[3] as? String
            }
            if let alertID = data[4] as? String {
                alertId = alertID
            }
        }
    }
    
}

struct ContactCategories {
   
    var categoryId : String?
    var categoryName : String?
    var categoryLanguage : String?
    
    init(data:[Any]) {
        
        if data.count >= 3 {
            categoryId = data[0] as? String
            categoryLanguage = data[1] as? String
            categoryName = data[2] as? String
        }
    }
}
/*[
    50.0,
    "2020-05-23",
    "ALERT",
    null,
    null
],*/

struct TransactionHistory {
    
    var amount : Float?
    var completedDateStr : String?
    var timeStr : String?
    var formattedCompletedDate : String?
    var date : Date!
    var transactionType : String?
    var cardNumber : Int?
    var collectionName : String?
    
    init(data:[Any]) {
        
        if data.count >= 5 {
            if let amountValue = data[0] as? Float {
                amount = amountValue
            }
            if let dateStr = data[1] as? String {
                completedDateStr = dateStr
                self.date = NSDate.getDateFromString(format: Constants.dateFormat.DATE_FORMAT, dateString: completedDateStr!)
                formattedCompletedDate = NSDate.getDateStringFromDate(format: Constants.dateFormat.DATE_FORMAT_UI, date: self.date!)
            }
            if let transacType = data[2] as? String {
                transactionType = transacType
            }
            if let cardNo = data[3] as? Int {
                cardNumber = cardNo
            }
            if let collName = data[4] as? String {
                collectionName = collName
            }
        }
    }
    
}

//MARK: Chat Object

struct ChatInfo {
    var message : String?
    var cardNumber : String?
    var action: String?
    var userName: String?
    var messageHeight : CGFloat?
    var timeStamp : TimeInterval?
    var messageDate : Date?

    init(chatInfoDict:Dictionary<String,Any>) {
        self.message = chatInfoDict["message"] as? String
        self.action = chatInfoDict["action"] as? String
        self.cardNumber = chatInfoDict["cardNum"] as? String
        self.userName = chatInfoDict["receiverUsername"] as? String
        self.timeStamp = chatInfoDict["timeInterval"] as? TimeInterval
        self.messageDate = Date(timeIntervalSince1970: self.timeStamp!)

        if let height = self.message {
            self.messageHeight = height.heightWithConstrainedWidth(text: height, width: UIScreen.main.bounds.size.width-80.0, font: UIFont.systemFont(ofSize: 17.0))
        }
        
    }
}
/*    [
    "chattest",
    8,
    "How are you?\n",
    1593536558529
],*/

struct RecentChat {
    
    var receiverName : String?
    var cardNumber : Int?
    var lastMessage : String?
    var lastMessageDate : Date?
    var lastMsgDateStr : String?

    init(recentChatInfo:[Any]) {
        if recentChatInfo.count >= 4 {
            self.receiverName = recentChatInfo[0] as? String
            self.cardNumber = recentChatInfo[1] as? Int
            self.lastMessage = recentChatInfo[2] as? String
            if let timeInterVal = recentChatInfo[3] as? TimeInterval {
                self.lastMessageDate = Date(timeIntervalSince1970: timeInterVal)
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = .current
                self.lastMsgDateStr = dateFormatter.string(from: self.lastMessageDate!)
            }
        }
    }
}


class CommunityClasses: NSObject {

    class func getCollectionsInfo(collectionInfo:[[Any]]) -> [CommunityCollection] {
        
        var collectionsArray : [CommunityCollection] = []
        
        for collectionData in collectionInfo {
            
            let communityCollection = CommunityCollection.init(data: collectionData)
            collectionsArray.append(communityCollection)
        }
        
        return collectionsArray
    }
    
    class func getCellectionDetailInfo(collecionDetailInfo:[[Int]]) -> [CollectionDetail] {
        
        var collectionDetailArray : [CollectionDetail] = []
        
        for collectionDetail in collecionDetailInfo {
            
            let collectionDetailObj = CollectionDetail.init(data: collectionDetail)
            collectionDetailArray.append(collectionDetailObj)
        }
        
        return collectionDetailArray
        
    }
    
    class func getCardDetailInfo(cardDetailInfo:[Dictionary<String,Any>]) -> [CardDetail] {
        
        var cardDetailArray : [CardDetail] = []
        
        for cardDetail in cardDetailInfo {
            
            let cardDetailObj = CardDetail.init(dataInfo: cardDetail)
            cardDetailArray.append(cardDetailObj)
        }
        
        return cardDetailArray
        
    }
    
    class func getAlertHistory(alertHistoryInfo:[[Any]]) -> [AlertDetail]{
        
        var alertHistoryArray : [AlertDetail] = []
        
        for alertInfo in alertHistoryInfo {
            
            let alertDetail = AlertDetail.init(data: alertInfo)
            alertHistoryArray.append(alertDetail)
        }
        
        return alertHistoryArray
        
    }
    
    class func getContactCategories(categoriesInfo:[[Any]]) -> [ContactCategories]{
        
        var categoriesArray : [ContactCategories] = []
        
        for catInfo in categoriesInfo {
            
            let categoryInfo = ContactCategories.init(data: catInfo)
            categoriesArray.append(categoryInfo)
        }
        
        return categoriesArray
        
    }
    // Get Transaction History Array
    class func getTransactionHistory(historyInfo:[[Any]]) -> [TransactionHistory] {
        
        var transHistoryArray : [TransactionHistory] = []
        
        for transInfo in historyInfo {
            
            let transactionHistory = TransactionHistory.init(data: transInfo)
            transHistoryArray.append(transactionHistory)
        }
        
        return transHistoryArray
    }
    
    //MARK: Get Recent Chats Info
    class func getUserRecentChatsInfo(recentChatInfo:[[Any]]) -> [RecentChat] {
    
        var recentChatArray : [RecentChat] = []
        
        for recentChat in recentChatInfo {
            
            let recentChatObj = RecentChat.init(recentChatInfo: recentChat)
            recentChatArray.append(recentChatObj)
        }
        
        return recentChatArray
    }
    
    class func getChatHistory(chatHistoryInfo:[[Any]]) -> [ChatInfo] {
        
        var chatHistoryArray : [ChatInfo] = []
        
        for chatInfo in chatHistoryInfo {
            let chatInfoDict = self.getChatHistoryDictInfo(chatInfo: chatInfo)
            let chatObj = ChatInfo.init(chatInfoDict: chatInfoDict)
            chatHistoryArray.append(chatObj)
        }
        
        return chatHistoryArray
        
    }
    
    /*    [
        "arun",
        "mukesh",
        "Hi mukesh",
        1592467282716
    ]*/
    
    class func getChatHistoryDictInfo(chatInfo: [Any]) -> Dictionary<String,Any>{
        
        var infoDict : [String:Any] = [:]
        let userName = UserDefaults.standard.value(forKey: Constants.STRINGS.USERNAME) as! String
        if chatInfo.count > 0 {
            if userName == chatInfo[0] as! String {
                infoDict["action"] = "sendMessage"
                infoDict["receiverUsername"] = chatInfo[0] as! String
            }else {
                infoDict["action"] = "receiveMessage"
                infoDict["receiverUsername"] = chatInfo[1] as! String
            }
            infoDict["message"] = chatInfo[2] as! String
            infoDict["cardNum"] = 0
            infoDict["timeInterval"] = chatInfo[3] as! TimeInterval
        }
        return infoDict
    }
 
}
