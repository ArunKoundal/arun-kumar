//
//  Constants.swift
//  ComunityCards
//
//  Created by Bunty on 05/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import Foundation
import Alamofire

enum AlertType : Int {
    case Insert = 1
    case Create = 2
    case Cancel = 3
}

enum DefaultKeys {
    
  static let publishableKey = "pk_test_Z1a6lVQbhK1XpnXvtXwU1tP400aVWD1R1k"
  static let baseURLString = "https://api.stripe.com/v1/charges"
  static let defaultCurrency = "inr" //"usd"
  static let secretKey = "sk_test_Z8wIu8iJGZxDhhTcM4Hhof6H00wOvcKaaR"
  static let defaultDescription = "Purchase from Community Card iOS"

}

struct Constants {
    
    //MARK: Check Network Availability
    struct Connectivity {
        static let sharedInstance = NetworkReachabilityManager()!
        static var isConnectedToInternet:Bool {
            return self.sharedInstance.isReachable
        }
    }
    
    //MARK:--- App Debug Mode

    struct DEBUG {
        static let print = true
    }
    
    struct STRINGS {
        static let TOKEN = "AccessToken"
        static let USERNAME = "UserName"
        static let USERID = "UserId"
        static let DEVICE_TOKEN = "deviceToken"

    }
    
    struct COLOR {
        static let DefaultAppColor = "#49A02E"
        static let CreateAlertColor = "#F6B143"
        static let viewLightGrey = "#DADADA"
        static let RemoveAlertColor = "#903A39"

    }
    
    //MARK:--- Date formats
    struct dateFormat {
        static let DATE_FORMAT = "yyyy-MM-dd"
        static let DATE_FORMAT_UI = "dd MMM yy"
        static let TIME_FORMAT = "hh:mm a"
    }
    
}
