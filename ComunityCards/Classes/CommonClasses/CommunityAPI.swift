//
//  CommunityAPI.swift
//  ComunityCards
//
//  Created by Bunty on 06/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import Foundation

struct BASEURL {
    
    static let URL = "https://jz00ltiiwe.execute-api.ap-south-1.amazonaws.com/v1/"
    
    struct API {
        
        static let LOGIN = "signin"
        static let REGISTER = "insertuser"
        static let TOKEN_VALIDATION = "authtest"
        static let GETCOLLECTION = "getcollectionsbyregion?region="
        static let GETCOLLECTION_DETAIL = "getcollectionsdetails?"
        static let GETCARD_DETAIL = "getcarddetails?"
        static let ISUSER_EXIST = "isusernameexists?username="
        static let INSERT_CARDS = "insertcard"
        static let ALERT_HISTORY = "getalertshistory?userid="
        static let CREATE_ALERT = "createalert"
        static let GETCONTACT_CATEGORIES = "getcontactcategories"
        static let INSERT_CONTACT_FORM = "insertcontactusformdata"
        static let GETUSER_TOTAL_AMOUNT = "gettotalamountperuser?"
        static let CREATEPAYMENT = "createpayment"
        static let CREATE_CHAT_CONNECTION = "wss://87yit2pdm8.execute-api.ap-south-1.amazonaws.com/v1?idToken="
        static let GETTRANSACTIONHISTORY = "gettransactions?userid="
        static let FORGOTPASSWORD_VERIFICATIONCODE = "forgotpassword"
        static let RESET_PASSWORD = "resetpassword"
        static let GET_CHAT_HISTORY = "getchathistory?"
        static let GET_RECENT_CHAT = "getrecentchats"
        static let REGISTER_DEVICE_TOKEN = "registerfornotification"
        static let REMOVE_ALERT_HISTORY = "removealerthistory"

    }

}


class CommunityAPI: NSObject {
    
    // Get Collection Detail
    class func getCollectionDetail(data:Dictionary<String,Any>, completion: @escaping ((_ result: [CollectionDetail]?,_ error : String) -> Void)){
    
        if !Constants.Connectivity.isConnectedToInternet {
            completion(nil, APIError.ErrorMessages.Default.NoInternet)
            return
        }
        let urlStr = BASEURL.URL + BASEURL.API.GETCOLLECTION_DETAIL + "lat=\(data["lat"] as! Double)" + "&long=\(data["long"] as! Double)" + "&collectionid=\(data["collectionid"] as! String)" + "&userid=\(data["userid"] as! String)"
        if Constants.DEBUG.print { print("Url is \(urlStr)") }
        
        var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
        var accessToken = ""
        if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
            accessToken = token as! String
        }
        request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")

        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let httpResponse = response as? HTTPURLResponse else {
            completion(nil, APIError.ErrorMessages.Default.RequestFailed)
              return
          }
          if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
              if let data = data {
                  do {
                    print(String(data: data, encoding: .utf8)!)
                    let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as! [[Int]]
                    let collectionDetailArray = CommunityClasses.getCellectionDetailInfo(collecionDetailInfo: tempArray)
                    completion(collectionDetailArray,"")
                  } catch {
                      print(error.localizedDescription)
                    completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                  }
              } else {
                completion(nil, APIError.ErrorMessages.Default.RequestFailed)
              }
          } else {
            completion(nil, APIError.ErrorMessages.Default.ServerError)
          }
        }
        task.resume()
    }
    
    //Check Token Validity
   class func checkTokenValidation(completion: @escaping ((_ result: Bool,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
            completion(false, APIError.ErrorMessages.Default.NoInternet)
            return
        }
        let urlStr = BASEURL.URL + BASEURL.API.TOKEN_VALIDATION
        if Constants.DEBUG.print { print("Url is \(urlStr)") }
        
        var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
        var accessToken = ""
        if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
            accessToken = token as! String
        }
        request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let httpResponse = response as? HTTPURLResponse else {
            completion(false, APIError.ErrorMessages.Default.RequestFailed)
              return
          }
          if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
              if let data = data {
                    if Constants.DEBUG.print { print(String(data: data, encoding: .utf8)!)}
                    completion(true,"")
              } else {
                completion(false, APIError.ErrorMessages.Default.RequestFailed)
              }
          } else {
            completion(false, APIError.ErrorMessages.Default.ServerError)
          }
        }
        task.resume()
    }
    
    //Card Detail
    class func getCardDetail(data:Dictionary<String,Any>,completion: @escaping ((_ result: [CardDetail]?,_ error : String) -> Void)){

        if !Constants.Connectivity.isConnectedToInternet {
             completion(nil, APIError.ErrorMessages.Default.NoInternet)
             return
         }
//        let urlStr =  "https://jz00ltiiwe.execute-api.ap-south-1.amazonaws.com/v1/getcarddetails?lat=38&long=-8&cardnumber=4%0A"

         let urlStr = BASEURL.URL + BASEURL.API.GETCARD_DETAIL + "lat=\(data["lat"] as! Double)" + "&long=\(data["long"] as! Double)" + "&cardnumber=\(data["cardnumber"] as! Int)"
         if Constants.DEBUG.print { print("Url is \(urlStr)") }
         
         var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
         var accessToken = ""
         if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
             accessToken = token as! String
         }
         request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")

         request.httpMethod = "GET"

         let task = URLSession.shared.dataTask(with: request) { data, response, error in
           guard let httpResponse = response as? HTTPURLResponse else {
             completion(nil, APIError.ErrorMessages.Default.RequestFailed)
               return
           }
           if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
               if let data = data {
                   do {
                     print(String(data: data, encoding: .utf8)!)
                     let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as! [Dictionary<String,Any>]
                     let cardDetailArray = CommunityClasses.getCardDetailInfo(cardDetailInfo: tempArray)
                     completion(cardDetailArray,"")
                   } catch {
                       print(error.localizedDescription)
                     completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                   }
               } else {
                 completion(nil, APIError.ErrorMessages.Default.RequestFailed)
               }
           } else {
             completion(nil, APIError.ErrorMessages.Default.ServerError)
           }
         }
         task.resume()
    }
    
    //Get User ID
    class func getUserID(userName:String,completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
             completion("", APIError.ErrorMessages.Default.NoInternet)
             return
         }
         let urlStr = BASEURL.URL + BASEURL.API.ISUSER_EXIST + "\(userName)"
         if Constants.DEBUG.print { print("Url is \(urlStr)") }
         
         var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
         var accessToken = ""
         if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
             accessToken = token as! String
         }
         request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
         request.httpMethod = "GET"

         let task = URLSession.shared.dataTask(with: request) { data, response, error in
           guard let httpResponse = response as? HTTPURLResponse else {
             completion("", APIError.ErrorMessages.Default.RequestFailed)
               return
           }
           if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
               if let data = data {
                let d = String(data: data, encoding: .utf8)!
                   do {
                     print(String(data: data, encoding: .utf8)!)
                     let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as! [[String]]
                    if tempArray.count > 0 {
                        let userIdArray = tempArray[0]
                        completion(userIdArray[0],"")
                    } else {
                         completion("", "No user Found")
                    }
                   } catch {
                       print(error.localizedDescription)
                     completion("", "\(d)RequestFailed")
                   }
               } else {
                 completion("", "-------\( httpResponse.statusCode)-after d")
               }
           } else {
             completion("", "APIError.ErrorMessages.Default.ServerError")
           }
         }
         task.resume()
    }
    
    //Insert Cards
    class func insertCards(postData:Data,completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
              completion("", APIError.ErrorMessages.Default.NoInternet)
              return
          }
          let urlStr = BASEURL.URL + BASEURL.API.INSERT_CARDS
          if Constants.DEBUG.print { print("Url is \(urlStr)") }
          
          var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
          var accessToken = ""
          if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
              accessToken = token as! String
          }
          request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
          request.httpMethod = "POST"
          request.httpBody = postData

          let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
              completion("", APIError.ErrorMessages.Default.RequestFailed)
                return
            }
            if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                
                if let data = data {
                    
                     let responseStr = String(data: data, encoding: .utf8)!
                     if responseStr.count > 0 {
                         completion(responseStr,"")
                     }

                } else {
                  completion("", APIError.ErrorMessages.Default.RequestFailed)
                }
            } else {
              completion("", APIError.ErrorMessages.Default.ServerError)
            }
          }
          task.resume()
    }
    
    // Create Alert
    class func createAlert(postData:Data,completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
              completion("", APIError.ErrorMessages.Default.NoInternet)
              return
          }
          let urlStr = BASEURL.URL + BASEURL.API.CREATE_ALERT
          if Constants.DEBUG.print { print("Url is \(urlStr)") }
          
          var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
          var accessToken = ""
          if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
              accessToken = token as! String
          }
          request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
          request.httpMethod = "POST"
          request.httpBody = postData

          let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
              completion("", APIError.ErrorMessages.Default.RequestFailed)
                return
            }
            if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                
                if let data = data {
                    
                     let responseStr = String(data: data, encoding: .utf8)!
                     if responseStr.count > 0 {
                         completion(responseStr,"")
                     }

                } else {
                  completion("", APIError.ErrorMessages.Default.RequestFailed)
                }
            } else {
              completion("", APIError.ErrorMessages.Default.ServerError)
            }
          }
          task.resume()
        
    }
    
    //Get Alert History
    class func getAlertHistory(completion: @escaping ((_ result: [AlertDetail]?,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
              completion(nil, APIError.ErrorMessages.Default.NoInternet)
              return
          }
          var userID = ""
          if let userId = UserDefaults.standard.value(forKey: Constants.STRINGS.USERID) {
            userID = userId as! String
          }
          let urlStr = BASEURL.URL + BASEURL.API.ALERT_HISTORY + "\(userID)"
          if Constants.DEBUG.print { print("Url is \(urlStr)") }
          
          var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
          var accessToken = ""
          if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
              accessToken = token as! String
          }
          request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
          request.httpMethod = "GET"

          let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
              completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                return
            }
            if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                
                if let data = data {
                    
                     do {
                       let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as! [[Any]]
                        if Constants.DEBUG.print { print("Info Array is \(tempArray)")}
                       let alertIntoArray = CommunityClasses.getAlertHistory(alertHistoryInfo: tempArray)
                       completion(alertIntoArray,"")
                     } catch {
                        if Constants.DEBUG.print { print(error.localizedDescription) }
                       completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                     }

                } else {
                  completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                }
            } else {
              completion(nil, APIError.ErrorMessages.Default.ServerError)
            }
          }
          task.resume()
    }
    
    // GetContactCategories
    class func getContactCategories(completion: @escaping ((_ result: [ContactCategories]?,_ error : String) -> Void)) {
        
    if !Constants.Connectivity.isConnectedToInternet {
            completion(nil, APIError.ErrorMessages.Default.NoInternet)
            return
        }
        let urlStr = BASEURL.URL + BASEURL.API.GETCONTACT_CATEGORIES
        if Constants.DEBUG.print { print("Url is \(urlStr)") }
        
        var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
        var accessToken = ""
        if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
            accessToken = token as! String
        }
        request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let httpResponse = response as? HTTPURLResponse else {
            completion(nil, APIError.ErrorMessages.Default.RequestFailed)
              return
          }
          if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
              
              if let data = data {
                  
                   do {
                     let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as! [[Any]]
                      if Constants.DEBUG.print { print("Info Array is \(tempArray)")}
                     let catIntoArray = CommunityClasses.getContactCategories(categoriesInfo: tempArray)
                     completion(catIntoArray,"")
                   } catch {
                      if Constants.DEBUG.print { print(error.localizedDescription) }
                     completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                   }

              } else {
                completion(nil, APIError.ErrorMessages.Default.RequestFailed)
              }
          } else {
            completion(nil, APIError.ErrorMessages.Default.ServerError)
          }
        }
        task.resume()
    }
    
    //Post Contact Us Form
    class func insertContactUsForm(postData:Data,completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
              completion("", APIError.ErrorMessages.Default.NoInternet)
              return
          }
          let urlStr = BASEURL.URL + BASEURL.API.INSERT_CONTACT_FORM
          if Constants.DEBUG.print { print("Url is \(urlStr)") }
          
          var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
          var accessToken = ""
          if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
              accessToken = token as! String
          }
          request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
          request.httpMethod = "POST"
          request.httpBody = postData

          let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
              completion("", APIError.ErrorMessages.Default.RequestFailed)
                return
            }
            if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                
                if let data = data {
                    
                     let responseStr = String(data: data, encoding: .utf8)!
                     if responseStr.count > 0 {
                         completion(responseStr,"")
                     }

                } else {
                  completion("", APIError.ErrorMessages.Default.RequestFailed)
                }
            } else {
              completion("", APIError.ErrorMessages.Default.ServerError)
            }
          }
          task.resume()
    }
    
    // GET User Total Amount
    class func getUserTotalAmount(completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
            completion("", APIError.ErrorMessages.Default.NoInternet)
            return
        }
        var userID = ""
        if let userId = UserDefaults.standard.value(forKey: Constants.STRINGS.USERID){
            userID = userId as! String
        }
        let urlStr = BASEURL.URL + BASEURL.API.GETUSER_TOTAL_AMOUNT + "userid=\(userID)"
        if Constants.DEBUG.print { print("Url is \(urlStr)") }
        
        var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
        var accessToken = ""
        if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
            accessToken = token as! String
        }
        request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")

        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let httpResponse = response as? HTTPURLResponse else {
            completion("", APIError.ErrorMessages.Default.RequestFailed)
              return
          }
          if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
              if let data = data {
                  do {
                    print(String(data: data, encoding: .utf8)!)
                    let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as! [[Double]]
                    var amountString = ""
                    if tempArray.count > 0 {
                        let infoArray = tempArray[0]
                        let tempAmount = infoArray[0]
                        amountString = String(tempAmount)
                    }
                    completion(amountString,"")
                  } catch {
                      print(error.localizedDescription)
                    completion("", APIError.ErrorMessages.Default.RequestFailed)
                  }
              } else {
                completion("", APIError.ErrorMessages.Default.RequestFailed)
              }
          } else {
            completion("", APIError.ErrorMessages.Default.ServerError)
          }
        }
        task.resume()
    }
    //Create Payment Call
    class func createPayment(postData:Data,completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
              completion("", APIError.ErrorMessages.Default.NoInternet)
              return
          }
          let urlStr = BASEURL.URL + BASEURL.API.CREATEPAYMENT
          if Constants.DEBUG.print { print("Url is \(urlStr)") }
          
          var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
          var accessToken = ""
          if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
              accessToken = token as! String
          }
          request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
          request.httpMethod = "POST"
          request.httpBody = postData

          let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
              completion("", APIError.ErrorMessages.Default.RequestFailed)
                return
            }
            if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                
                if let data = data {
                    
                     let responseStr = String(data: data, encoding: .utf8)!
                     if responseStr.count > 0 {
                         completion(responseStr,"")
                     }

                } else {
                  completion("", APIError.ErrorMessages.Default.RequestFailed)
                }
            } else {
              completion("", APIError.ErrorMessages.Default.ServerError)
            }
          }
          task.resume()
    }
    
    //Get Transaction History Call
    class func getTransactionHistory(completion: @escaping ((_ result: [TransactionHistory]?,_ error : String) -> Void)) {
        
           if !Constants.Connectivity.isConnectedToInternet {
               completion(nil, APIError.ErrorMessages.Default.NoInternet)
               return
           }
            var userID = ""
            if let userId = UserDefaults.standard.value(forKey: Constants.STRINGS.USERID){
                userID = userId as! String
            }
           let urlStr = BASEURL.URL + BASEURL.API.GETTRANSACTIONHISTORY + "\(userID)"
           if Constants.DEBUG.print { print("Url is \(urlStr)") }
           
           var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
           var accessToken = ""
           if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
               accessToken = token as! String
           }
           request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
           request.httpMethod = "GET"

           let task = URLSession.shared.dataTask(with: request) { data, response, error in
             guard let httpResponse = response as? HTTPURLResponse else {
               completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                 return
             }
             if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                 
                 if let data = data {
                     
                      do {
                        let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as! [[Any]]
                         if Constants.DEBUG.print { print("Info Array is \(tempArray)")}
                        let transactionHistoryArray = CommunityClasses.getTransactionHistory(historyInfo: tempArray)
                        completion(transactionHistoryArray,"")
                      } catch {
                         if Constants.DEBUG.print { print(error.localizedDescription) }
                        completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                      }

                 } else {
                   completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                 }
             } else {
               completion(nil, APIError.ErrorMessages.Default.ServerError)
             }
        }
        task.resume()
    }
    //Get ForgetPassword Verification Code
    class func getForgotPasswordVerificationCode(username: String, completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
              completion("", APIError.ErrorMessages.Default.NoInternet)
              return
          }
          let urlStr = BASEURL.URL + BASEURL.API.FORGOTPASSWORD_VERIFICATIONCODE
          if Constants.DEBUG.print { print("Url is \(urlStr)") }
          
          var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
          let parameters = "username=\(username)"
          let postData =  parameters.data(using: .utf8)
          request.httpMethod = "POST"
          request.httpBody = postData

          let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
              completion("", APIError.ErrorMessages.Default.RequestFailed)
                return
            }
            if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                
                if let data = data {
                    
                     let responseStr = String(data: data, encoding: .utf8)!
                     if responseStr.count > 0 {
                         completion(responseStr,"")
                     }

                } else {
                  completion("", APIError.ErrorMessages.Default.RequestFailed)
                }
            } else {
                if let data = data {
                     let responseStr = String(data: data, encoding: .utf8)!
                     if responseStr.count > 0 {
                         completion("", responseStr)
                     }
                }
            }
          }
        task.resume()
    }
    
    // Reset Password
    class func resetPasswordWithVerificationCode(username: String,token: String,password: String, completion: @escaping ((_ result: String,_ error : String) -> Void)) {
         
         if !Constants.Connectivity.isConnectedToInternet {
               completion("", APIError.ErrorMessages.Default.NoInternet)
               return
           }
           let urlStr = BASEURL.URL + BASEURL.API.RESET_PASSWORD
           if Constants.DEBUG.print { print("Url is \(urlStr)") }
           
           var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
           let parameters = "username=\(username)&token=\(token)&password=\(password)"
           let postData =  parameters.data(using: .utf8)
           request.httpMethod = "POST"
           request.httpBody = postData

           let task = URLSession.shared.dataTask(with: request) { data, response, error in
             guard let httpResponse = response as? HTTPURLResponse else {
               completion("", APIError.ErrorMessages.Default.RequestFailed)
                 return
             }
             if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                 
                 if let data = data {
                     
                      let responseStr = String(data: data, encoding: .utf8)!
                      if responseStr.count > 0 {
                          completion(responseStr,"")
                      }

                 } else {
                   completion("", APIError.ErrorMessages.Default.RequestFailed)
                 }
             } else {
                if let data = data {
                     let responseStr = String(data: data, encoding: .utf8)!
                     if responseStr.count > 0 {
                         completion("", responseStr)
                     }
                }
             }
           }
        task.resume()
     }
    
  //Get CHAT History
  class func getChatHistory(chatInfo:String,completion: @escaping ((_ result: [ChatInfo]?,_ error : String) -> Void)) {

       if !Constants.Connectivity.isConnectedToInternet {
            completion(nil, APIError.ErrorMessages.Default.NoInternet)
            return
        }
        var urlStr = BASEURL.URL + BASEURL.API.GET_CHAT_HISTORY + "\(chatInfo)"
        urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        if Constants.DEBUG.print { print("Url is \(urlStr)") }
        var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
        var accessToken = ""
        if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
            accessToken = token as! String
        }
        request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let httpResponse = response as? HTTPURLResponse else {
            completion(nil, APIError.ErrorMessages.Default.RequestFailed)
              return
          }
          if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
              if let data = data {
                  do {
                    print(String(data: data, encoding: .utf8)!)
                    let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as! [[Any]]
                   if tempArray.count > 0 {
                    let chatHistoryArray = CommunityClasses.getChatHistory(chatHistoryInfo: tempArray)
                        completion(chatHistoryArray,"")
                   } else {
                      completion(nil, APIError.ErrorMessages.Default.NoDataFound)
                    }
                  } catch {
                      print(error.localizedDescription)
                    completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                  }
              } else {
                completion(nil, APIError.ErrorMessages.Default.RequestFailed)
              }
          } else {
            completion(nil, APIError.ErrorMessages.Default.ServerError)
          }
       }
        task.resume()
    }
    
    //MARK: Get Recent Chats
    class func getRecentChats(completion: @escaping ((_ result: [RecentChat]?,_ error : String) -> Void)) {

         if !Constants.Connectivity.isConnectedToInternet {
              completion(nil, APIError.ErrorMessages.Default.NoInternet)
              return
          }
          let urlStr = BASEURL.URL + BASEURL.API.GET_RECENT_CHAT
          if Constants.DEBUG.print { print("Url is \(urlStr)") }
          
          var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
          var accessToken = ""
          if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
              accessToken = token as! String
          }
          request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
          request.httpMethod = "GET"

          let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
              completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                return
            }
            if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
                if let data = data {
                    do {
                      print(String(data: data, encoding: .utf8)!)
                      let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as! [[Any]]
                     if tempArray.count > 0 {
                        let recentChatArray = CommunityClasses.getUserRecentChatsInfo(recentChatInfo: tempArray)
                        completion(recentChatArray,"")
                     } else {
                        completion(nil, "No record found")
                        }
                    } catch {
                        print(error.localizedDescription)
                      completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                    }
                } else {
                  completion(nil, APIError.ErrorMessages.Default.RequestFailed)
                }
            } else {
              completion(nil, APIError.ErrorMessages.Default.ServerError)
            }
         }
          task.resume()
      }
    
  //MARK: Create Charge API
  class func createChargeFromCards(amount: Int,token:String,completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
        if !Constants.Connectivity.isConnectedToInternet {
             completion("", APIError.ErrorMessages.Default.NoInternet)
             return
         }
        let urlStr = DefaultKeys.baseURLString
        if Constants.DEBUG.print { print("Url is \(urlStr)") }
        let parameters = "source=\(token)&amount=\(amount)&currency=\(DefaultKeys.defaultCurrency)&description=\(DefaultKeys.defaultDescription)"
        if Constants.DEBUG.print { print("Params Info is \(parameters)") }

        let postData =  parameters.data(using: .utf8)

        var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(DefaultKeys.secretKey)", forHTTPHeaderField: "Authorization")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let httpResponse = response as? HTTPURLResponse else {
            completion("", APIError.ErrorMessages.Default.RequestFailed)
              return
          }
          if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
              if let data = data {
                  do {
                    if Constants.DEBUG.print { print(String(data: data, encoding: .utf8)!) }
                    let tempDict = try JSONSerialization.jsonObject(with: data, options:[]) as! [String:Any]
                    if tempDict["id"] != nil {
                       completion("Success","")
                    }
                  } catch {
                      print(error.localizedDescription)
                    completion("", APIError.ErrorMessages.Default.RequestFailed)
                  }
              } else {
                completion("", APIError.ErrorMessages.Default.RequestFailed)
              }
          } else {
            completion("", APIError.ErrorMessages.Default.PaymentFailed)
          }
        }
        task.resume()
    }
    
    //MARK: Submit Device Token to Server
    class func submitDeviceTokenToServer(completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
    if !Constants.Connectivity.isConnectedToInternet {
           completion("", APIError.ErrorMessages.Default.NoInternet)
           return
       }
      let urlStr = BASEURL.URL + BASEURL.API.REGISTER_DEVICE_TOKEN
      if Constants.DEBUG.print { print("Url is \(urlStr)") }
      let deviceType = "ios"
      var deviceToken = ""
      if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.DEVICE_TOKEN) {
          deviceToken = token as! String
      }
      var userName = ""
      if let name = UserDefaults.standard.value(forKey: Constants.STRINGS.USERNAME) {
          userName = name as! String
       }
      let parameters = "deviceType=\(deviceType)&token=\(deviceToken)&username=\(userName)"
      if Constants.DEBUG.print { print("Params Info is \(parameters)") }

      let postData =  parameters.data(using: .utf8)

      var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
      var accessToken = ""
      if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
            accessToken = token as! String
       }
      request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
      request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
      request.httpMethod = "POST"
      request.httpBody = postData

      let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let httpResponse = response as? HTTPURLResponse else {
          completion("", APIError.ErrorMessages.Default.RequestFailed)
            return
        }
        if httpResponse.statusCode == 200 {
            completion("200","")
        } else {
          completion("", APIError.ErrorMessages.Default.RequestFailed)
        }
      }
      task.resume()
    }
    
    //MARK: Remove Alert from History
    class func removeAlertFromHistory(alertId:String,completion: @escaping ((_ result: String,_ error : String) -> Void)) {
        
    if !Constants.Connectivity.isConnectedToInternet {
           completion("", APIError.ErrorMessages.Default.NoInternet)
           return
       }
      let urlStr = BASEURL.URL + BASEURL.API.REMOVE_ALERT_HISTORY
      if Constants.DEBUG.print { print("Url is \(urlStr)") }
        
      let parameters = "alertid=\(alertId)"
      if Constants.DEBUG.print { print("Params Info is \(parameters)") }

      let postData =  parameters.data(using: .utf8)

      var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
      var accessToken = ""
      if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
            accessToken = token as! String
       }
      request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
      request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
      request.httpMethod = "POST"
      request.httpBody = postData

      let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let httpResponse = response as? HTTPURLResponse else {
          completion("", APIError.ErrorMessages.Default.RequestFailed)
            return
        }
        if httpResponse.statusCode == 200 || httpResponse.statusCode == 201 {
            if let data = data {
                let responseStr = String(data: data, encoding: .utf8)!
                if Constants.DEBUG.print { print(String(data: data, encoding: .utf8)!) }
                completion(responseStr,"")
            } else {
              completion("", APIError.ErrorMessages.Default.RequestFailed)
            }
        } else {
          completion("", APIError.ErrorMessages.Default.ServerError)
        }
      }
      task.resume()
    }
    
    
    
}


/*

class CommunityAPI {
    
    let register = BASEURL.URL + "insertuser"
    let login = BASEURL.URL + "signin"
    
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        self.session = URLSession(configuration: configuration)
    
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
}

extension CommunityAPI : APIClient {
    
    func postRegisterForm(postData:Data,completion: @escaping (Result<String?, APIError>) -> Void) {
        
        guard let url = URL(string: register) else {
                return
            }
        var request = URLRequest(url: url,timeoutInterval: Double.infinity)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = postData
        fetch(with: request, decode: { (decode) -> String in
             let token = decode as! String
            return token
            
        }, completion: completion)
    }
    
    func postLoginForm(postData:Data,completion: @escaping (Result<String?, APIError>) -> Void) {
        
        guard let url = URL(string: login) else {
                return
            }
        if Constants.DEBUG.print { print("URL is \(url)") }
        
        var request = URLRequest(url: url,timeoutInterval: Double.infinity)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = postData
        fetch(with: request, decode: { (decode) -> String in
             let token = decode as! String
            return token
            
        }, completion: completion)
    }
    
}
*/
