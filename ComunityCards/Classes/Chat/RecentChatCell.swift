//
//  RecentChatCell.swift
//  ComunityCards
//
//  Created by Arun on 05/07/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class RecentChatCell: UITableViewCell {
    
    @IBOutlet weak var receiverUserLbl : UILabel!
    @IBOutlet weak var carNumberLbl : UILabel!
    @IBOutlet weak var lastMsgLbl : UILabel!
    @IBOutlet weak var lastMsgDateLbl : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureRecentChatCell(recentChatInfo:RecentChat) {
        
        self.receiverUserLbl.text = recentChatInfo.receiverName
        if let cardNumber = recentChatInfo.cardNumber {
            self.carNumberLbl.text = "\(cardNumber)"
        }
        self.lastMsgLbl.text = recentChatInfo.lastMessage
        self.lastMsgDateLbl.text = recentChatInfo.lastMsgDateStr

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
