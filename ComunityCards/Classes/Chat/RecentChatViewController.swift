//
//  RecentChatViewController.swift
//  ComunityCards
//
//  Created by Arun on 05/07/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD

class RecentChatViewController: UIViewController {
    
    let recentChatIdentifier = "RecentChatCell"
    
    @IBOutlet weak var recentChatTableView : UITableView!
    var recentChatArray : [RecentChat] = []
    var appDelegate = AppDelegate()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpTableCell()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = false
        self.appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.appDelegate.screenStatus = false
        
        self.getUserRecentChatHistory()
    }
    
    //MARK: Get Recent Chat History
    
    func getUserRecentChatHistory() {
        
        SVProgressHUD.show()
        CommunityAPI.getRecentChats { (recentChatInfo, error) in
            SVProgressHUD.dismiss()
            if error.count == 0 {
                 if Constants.DEBUG.print { print("Recent Chat Array is \(recentChatInfo!)") }
                self.recentChatArray = recentChatInfo!
                DispatchQueue.main.async {
                    self.recentChatTableView.reloadData()
                }
            }else {
                
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                }
            }
        }
        
    }
    
    //MARK: Set Up Table Cell
    func setUpTableCell() {
        
        let recentChatCell = UINib(nibName: "RecentChatCell", bundle: .main)
        self.recentChatTableView.register(recentChatCell, forCellReuseIdentifier: recentChatIdentifier)
    }

}

extension RecentChatViewController : UITableViewDataSource, UITableViewDelegate {
    
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78.0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recentChatArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: recentChatIdentifier) as! RecentChatCell
        cell.configureRecentChatCell(recentChatInfo: self.recentChatArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.recentChatTableView.deselectRow(at: indexPath, animated: true)
        
        let chatViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatViewController.screenStatus = true
        let recentChat = self.recentChatArray[indexPath.row]
        chatViewController.cardNumber = recentChat.cardNumber!
        if let userName = recentChat.receiverName {
            chatViewController.receiverUserName = userName
        }
        self.navigationController?.pushViewController(chatViewController, animated: true)
    }
    
}
