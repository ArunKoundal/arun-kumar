//
//  ChatViewController.swift
//  ComunityCards
//
//  Created by Arun Kumar on 27/04/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD

import Starscream

class ChatViewController: UIViewController {

    let chatCellIdentifier = "ChatCustomCell"
    
    var socket: WebSocket!
    var isConnected = false
    let server = WebSocketServer()
    @IBOutlet weak var msgTextView: UITextView!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var loadMoreView: UIView!
    @IBOutlet weak var cardView: UIView!

    var chatMessagesArray : [ChatInfo] = []
    var receiverUserName : String = ""
    var cardNumber : Int = 0
    var screenStatus: Bool = false
    var timeStamp = TimeInterval()
    var lastElement : Int = -1
    var loadFirstTime : Bool = false
    
    var appDelegate = AppDelegate()
    
    //MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.appDelegate = UIApplication.shared.delegate as! AppDelegate

        self.view.backgroundColor = UIColor.white
        self.registerTableCell()
        self.makeServerSocketConnection()
        self.keyboardObserverNotifications()
        self.addToolBarOnKeyboard()
        self.setUpViews()
        self.getChatHistory(loadStatus: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        self.setUpNavigationItemViews()
    }
    
    //MARK: Set Up View
    func setUpNavigationItemViews() {
        
        if self.screenStatus == false {
            self.navigationItem.leftBarButtonItem = nil
            self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.clear
        }
        //self.cardView.layer.masksToBounds = true
        //self.cardView.layer.cornerRadius = self.cardView.frame.size.width/2
    }
    
    func setUpViews() {
        
        if self.screenStatus == false {
            self.tabBarController?.tabBar.isHidden = false
        }else {
            self.tabBarController?.tabBar.isHidden = true
        }
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.cardNumberLabel.text = "\(self.cardNumber)"
        self.userNameLabel.text = "\(self.receiverUserName)"
        self.chatTableView.estimatedRowHeight = 50.0
        self.chatTableView.rowHeight = UITableView.automaticDimension
        self.setUpTextViewContainer()
        
    }
    
    //MARK: SetUpTextContainerView
    func setUpTextViewContainer() {
        
        if self.appDelegate.screenStatus == true {
            self.setTextContainerView_Inside()
        }else {
            self.setTextContainerView_OutSide()
        }
        
    }
    
    func setTextContainerView_Inside() {
        
        self.containerView.isHidden = false
        self.chatTableView.translatesAutoresizingMaskIntoConstraints = true
        if UIScreen.main.bounds.size.height >= 812 {
            self.chatTableView.frame = CGRect(x:self.chatTableView.frame.origin.x,y:140.0,width:self.view.frame.size.width,height:self.view.frame.size.height-234.0)
        }else {
            self.chatTableView.frame = CGRect(x:self.chatTableView.frame.origin.x,y:140.0,width:self.view.frame.size.width,height:self.view.frame.size.height-176.0)
        }
        if self.receiverUserName == "CC Alerts" {
            self.containerView.isHidden = true
            if UIScreen.main.bounds.size.height >= 812 {
                self.chatTableView.frame = CGRect(x:self.chatTableView.frame.origin.x,y:104.0,width:self.view.frame.size.width,height:self.view.frame.size.height-174.0)
            }else {
                self.chatTableView.frame = CGRect(x:self.chatTableView.frame.origin.x,y:104.0,width:self.view.frame.size.width,height:self.view.frame.size.height-116.0)
            }
        }
    }
    
    func setTextContainerView_OutSide() {
        
        self.containerView.isHidden = false
        self.chatTableView.translatesAutoresizingMaskIntoConstraints = true
        if UIScreen.main.bounds.size.height >= 812 {
            self.chatTableView.frame = CGRect(x:self.chatTableView.frame.origin.x,y:52.0,width:self.view.frame.size.width,height:self.view.frame.size.height-234.0)
        }else {
            self.chatTableView.frame = CGRect(x:self.chatTableView.frame.origin.x,y:52.0,width:self.view.frame.size.width,height:self.view.frame.size.height-176.0)
        }
        if self.receiverUserName == "CC Alerts" {
            self.containerView.isHidden = true
            if UIScreen.main.bounds.size.height >= 812 {
                self.chatTableView.frame = CGRect(x:self.chatTableView.frame.origin.x,y:52.0,width:self.view.frame.size.width,height:self.view.frame.size.height-174.0)
            }else {
                self.chatTableView.frame = CGRect(x:self.chatTableView.frame.origin.x,y:52.0,width:self.view.frame.size.width,height:self.view.frame.size.height-116.0)
            }
        }
        
    }
    
    
    //MARK: Set Up Table View Cell
    func registerTableCell() {
        
        let chatCell = UINib(nibName: "ChatTableViewCell", bundle: .main)
        self.chatTableView.register(chatCell, forCellReuseIdentifier: chatCellIdentifier)
    }
    
    //MARK: Get Chat History
    func getChatHistory(loadStatus:Bool) {
        SVProgressHUD.show()
        let infoStr = self.getChatHistoryInfoStr(loadPastMsg: loadStatus)
        CommunityAPI.getChatHistory(chatInfo: infoStr) { (chatInfoArray, error) in
            SVProgressHUD.dismiss()
            if error.count == 0 {
                if Constants.DEBUG.print { print("Chat History is \(chatInfoArray!)") }
                if chatInfoArray?.count == 0 {
                    //self.loadMoreView.isHidden = true
                    Utility.showAlertMessage(title: "Alert!", message: "No more past messages")
                }
                if chatInfoArray!.count > 0 {
                    var tempChatArray : [ChatInfo] = chatInfoArray!
                    tempChatArray.append(contentsOf: self.chatMessagesArray)
                    self.chatMessagesArray = tempChatArray
                    if loadStatus == true {
                        let indexPath = IndexPath(row: 0, section: 0)
                        DispatchQueue.main.async {
                            self.chatTableView.scrollToRow(at: indexPath, at: .top, animated: true)
                        }
                    }
                    //self.chatMessagesArray.append(contentsOf: chatInfoArray!)
                }
                DispatchQueue.main.async {
                    self.chatTableView.reloadData()
                }
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                }
            }
        }
    }
    
    //MARK: Get Chat History Parameters
    func getChatHistoryInfoStr(loadPastMsg:Bool) -> String {
        
        //"receiverUsername=mukesh&cardNum=8&time=1591725051"
        self.timeStamp = TimeInterval(Date().millisecondsSince1970) //NSDate().timeIntervalSince1970 //1592467282716
        if Constants.DEBUG.print { print("Time Stamp is \(timeStamp)") }
        //self.receiverUserName = self.receiverUserName.replacingOccurrences(of: " ", with: "")
        var infoStr = ""
        if loadPastMsg == false {
            infoStr = "receiverUsername=\(self.receiverUserName)&cardNum=\(self.cardNumber)&time=\(timeStamp)"
        }else {
            if self.chatMessagesArray.count > 0 {
                let pastStamp = self.chatMessagesArray[0].timeStamp
                infoStr = "receiverUsername=\(self.receiverUserName)&cardNum=\(self.cardNumber)&time=\(pastStamp!)"
            }
        }
        if Constants.DEBUG.print { print("Info String is \(infoStr)") }

        return infoStr
        
    }

    //MARK: Connect To Server Socket
    func makeServerSocketConnection() {
        var accessToken = ""
        if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
            accessToken = token as! String
        }
        let urlString = BASEURL.API.CREATE_CHAT_CONNECTION + accessToken
        if Constants.DEBUG.print { print("Url string is \(urlString)") }
        var request = URLRequest(url: URL(string: urlString)!) //https://localhost:8080
        request.timeoutInterval = 5
        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }

    // MARK: Send Message Action
    
    @IBAction func sendTextMessage(_ sender: Any) {
        
        if self.msgTextView.text.count > 0 {
            let messageStr = self.sendMessage()
            socket.write(string: messageStr)
            self.msgTextView.text = ""
            self.placeHolderLabel.isHidden = false
        }else {
            Utility.showAlertMessage(title: "Alert!", message: "Please write your message.")
        }

    }
    
    func sendMessage() -> String {
        
        var sendMsgDict : Dictionary<String,Any> = [:]
        sendMsgDict["action"] = "sendMessage"
        sendMsgDict["message"] = self.msgTextView.text
        sendMsgDict["receiverUsername"] = self.receiverUserName
        sendMsgDict["cardNum"] =  self.cardNumber
        sendMsgDict["timeInterval"] =  self.timeStamp

        let jsonData = try! JSONSerialization.data(withJSONObject: sendMsgDict, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        if Constants.DEBUG.print { print("Message is \(jsonString)") }
        
        let chatInfo = ChatInfo.init(chatInfoDict: sendMsgDict)
        self.chatMessagesArray.append(chatInfo)
        if Constants.DEBUG.print { print("Chat Info Array Count is \(self.chatMessagesArray.count)") }
        self.setTableScrollPosition()
      //  return #"{"action" : "sendMessage" , "message" : "Hello Arun - dashing123 ","receiverUsername":"arun","cardNum":"8"}"#
        return "{\"action\" : \"sendMessage\" , \"message\" : \"\(self.msgTextView.text ?? "") \",\"receiverUsername\":\"\(self.receiverUserName)\",\"cardNum\":\"\(self.cardNumber)\",\"timeInterval\":\"\(self.timeStamp)\"}"
    }
    
    //MARK: Set Table Scroll Position
    func setTableScrollPosition() {
        
        self.chatTableView.reloadData()

        if self.chatMessagesArray.count <= 5 {
            let indexPath = IndexPath(row: 0, section: 0)
            self.chatTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }else {
            let indexPath = IndexPath(row: self.chatMessagesArray.count-1, section: 0)
            self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }

    }
    
    // MARK: Disconnect Action
    
    @IBAction func backAction(_ sender: Any) {
        
        if isConnected {
            socket.disconnect()
        }
        self.appDelegate.screenStatus = false
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Keyboard Notifications
    
    func keyboardObserverNotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            self.containerView.translatesAutoresizingMaskIntoConstraints = true
//            self.chatTableView.translatesAutoresizingMaskIntoConstraints = true
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            var yAxisFrame = self.getContainerViewFrameWhenKeyboardWillShow(keyboardHeight: keyboardHeight)
            if self.appDelegate.screenStatus == false {
                yAxisFrame -= 80
            }
            self.containerView.frame = CGRect(x:0.0,y:yAxisFrame,width:UIScreen.main.bounds.size.width,height:self.containerView.frame.size.height)

            if UIScreen.main.bounds.size.height >= 812 {
                self.chatTableView.frame.size.height = UIScreen.main.bounds.size.height-(keyboardHeight + 60.0 + 52 + 88)
            }else {
                self.chatTableView.frame.size.height = UIScreen.main.bounds.size.height-(keyboardHeight + 60.0 + 52 + 64)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.containerView.translatesAutoresizingMaskIntoConstraints = true
        //self.view.frame.origin.y = 0
        var yAxisFrame = self.getContainerViewFrameWhenKeyboardWillHide()
        if self.appDelegate.screenStatus == false {
            yAxisFrame -= 80
        }
        self.containerView.frame = CGRect(x:0.0,y:yAxisFrame,width:UIScreen.main.bounds.size.width,height:self.containerView.frame.size.height)

        if UIScreen.main.bounds.size.height >= 812 {
            self.chatTableView.frame.size.height = UIScreen.main.bounds.size.height-(60.0 + 52 + 88)
        }else {
            self.chatTableView.frame.size.height = UIScreen.main.bounds.size.height-( 60.0 + 52 + 64)
        }

    }
    
    func getContainerViewFrameWhenKeyboardWillShow(keyboardHeight:CGFloat) -> CGFloat {
        
        var height : CGFloat = 0.0
        if UIScreen.main.bounds.size.height >= 812 {
            height = UIScreen.main.bounds.size.height - (70.0 + keyboardHeight)
        }else {
            height = UIScreen.main.bounds.size.height - (60.0 + keyboardHeight)
        }
        return height
    }
    
    func getContainerViewFrameWhenKeyboardWillHide() -> CGFloat {
        
        var height : CGFloat = 0.0
        if UIScreen.main.bounds.size.height >= 812 {
            height = UIScreen.main.bounds.size.height - 94.0
        }else {
            height = UIScreen.main.bounds.size.height - 60.0
        }
        return height
    }
    
    //MARK: Add Toolbar On Keyboard
    func addToolBarOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.msgTextView.inputAccessoryView = doneToolbar;

    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    //MAKR: Load More Chat
    
    @IBAction func loadMoreChat(_ sender:Any) {
        
        self.getChatHistory(loadStatus: true)
        
    }

}

//MARK: WebSocketDelegates

extension ChatViewController:WebSocketDelegate {
    
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            isConnected = true
            if Constants.DEBUG.print { print("websocket is connected: \(headers)")}

        case .disconnected(let reason, let code):
            isConnected = false
            if Constants.DEBUG.print { print("websocket is disconnected: \(reason) with code: \(code)")}
        case .text(let string):
            if Constants.DEBUG.print { print("Received text: \(string)")}
            self.getReceiveInfoDict(chatString: string)
        case .binary(let data):
            if Constants.DEBUG.print { print("Received data: \(data.count)")}
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            isConnected = false
        case .error(let error):
            isConnected = false
            handleError(error)
        }
    }
    
    func handleError(_ error: Error?) {
        if let e = error as? WSError {
            if Constants.DEBUG.print { print("websocket encountered an error: \(e.message)")}
        } else if let e = error {
            if Constants.DEBUG.print { print("websocket encountered an error: \(e.localizedDescription)")}
        } else {
            if Constants.DEBUG.print { print("websocket encountered an error")}
        }
    }
    
    //MARK: Get Receive Info Dict
    func getReceiveInfoDict(chatString:String) {
        
        var receiveMsgDict : Dictionary<String,Any> = [:]
        var tempInfoDict : Dictionary<String,Any> = [:]
        if let data = chatString.data(using: .utf8) {
            do {
                tempInfoDict = try (JSONSerialization.jsonObject(with: data, options: []) as? [String: Any])!
                if Constants.DEBUG.print { print("Dict info is \(tempInfoDict)") }
                if tempInfoDict["message"] as! String  == "Internal server error" {
                    return
                }
                receiveMsgDict["action"] = "receiveMessage"
                receiveMsgDict["message"] = tempInfoDict["message"] as! String
                receiveMsgDict["receiverUsername"] = tempInfoDict["sender"] as! String
                receiveMsgDict["cardNum"] = tempInfoDict["cardNumber"] as! String
                receiveMsgDict["timeInterval"] =  self.timeStamp

                let chatInfo = ChatInfo.init(chatInfoDict: receiveMsgDict)
                self.chatMessagesArray.append(chatInfo)
                if Constants.DEBUG.print { print("Chat Info Array count is \(self.chatMessagesArray.count)") }
                self.setTableScrollPosition()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
//   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let rowHeight = tableView.estimatedRowHeight
//        return rowHeight
//    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {

        let rowHeight = tableView.estimatedRowHeight
        return rowHeight
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatMessagesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: chatCellIdentifier) as! ChatTableViewCell
        cell.configureChatInfoCell(chatInfo: self.chatMessagesArray[indexPath.row])
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.chatTableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        if self.chatMessagesArray.count <= 50 {
//            return
//        }
//        if indexPath.row >= 50{
//            self.lastElement = 0
//        }
//        if Constants.DEBUG.print { print("Index Row is \(indexPath.row)") }
//        if indexPath.row == lastElement {
//            self.getChatHistory(loadStatus: true)
//        }
    }
    
}

//MARK: TextView Delegate methods

extension ChatViewController: UITextViewDelegate {
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        self.placeHolderLabel.isHidden = false
        if textView.text.count > 1 {
            self.placeHolderLabel.isHidden = true
        }
        return true
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        
        let textViewText = (textView.text as NSString)
        if textViewText.length<1 {
            self.placeHolderLabel.isHidden = false
            
        }else {
            self.placeHolderLabel.isHidden = true
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        
        let textViewText = (textView.text as NSString)
        if textViewText.length<1 {
            self.placeHolderLabel.isHidden = false
        }
    }
}

extension Float {
    var clean: String {
       return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

//   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let chatInfo = self.chatMessagesArray[indexPath.row]
//        if Constants.DEBUG.print { print("Row height is \(chatInfo.messageHeight! + 30.0)") }
//        return chatInfo.messageHeight! + 30.0
//    }
