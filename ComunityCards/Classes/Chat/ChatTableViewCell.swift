//
//  ChatTableViewCell.swift
//  ComunityCards
//
//  Created by Bunty on 26/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leftMsgView: UIView!
    @IBOutlet weak var rightMsgView: UIView!
    @IBOutlet weak var leftMsgLbl: UILabel!
    @IBOutlet weak var rightMsgLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Configure Chat Info Cell
    func configureChatInfoCell(chatInfo:ChatInfo) {
        
        if let actionType = chatInfo.action {
            if actionType == "sendMessage" { //receiveMessage
                self.leftMsgView.isHidden = true
                self.rightMsgView.isHidden = false
                if let message = chatInfo.message {
                    self.rightMsgLbl.text = message
                }
            }else {
                self.leftMsgView.isHidden = false
                self.rightMsgView.isHidden = true
                if let message = chatInfo.message {
                    self.leftMsgLbl.text = message
                }
            }
        }
       // self.setUpMessageView(chatInfo: chatInfo)
    }
    
    //MARK: Set Up Message Views
    func setUpMessageView(chatInfo:ChatInfo) {
        
        self.leftMsgView.translatesAutoresizingMaskIntoConstraints = true
        self.leftMsgLbl.translatesAutoresizingMaskIntoConstraints = true
        self.rightMsgView.translatesAutoresizingMaskIntoConstraints = true
        self.rightMsgLbl.translatesAutoresizingMaskIntoConstraints = true
        
        if let actionType = chatInfo.action {
            if actionType == "sendMessage" {
                self.rightMsgLbl.frame = CGRect(x:5.0,y:10.0,width:UIScreen.main.bounds.size.width-80.0,height:chatInfo.messageHeight!)
                self.rightMsgView.frame = CGRect(x:60.0,y:5.0,width:UIScreen.main.bounds.size.width-70.0,height:chatInfo.messageHeight! + 30.0)
            }else {
                self.leftMsgLbl.frame = CGRect(x:5.0,y:10.0,width:UIScreen.main.bounds.size.width-80.0,height:chatInfo.messageHeight!)
                self.leftMsgView.frame = CGRect(x:60.0,y:5.0,width:UIScreen.main.bounds.size.width-70.0,height:chatInfo.messageHeight! + 30.0)
           }
       }
    }
    
}
