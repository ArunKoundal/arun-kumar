//
//  StripeViewController.swift
//  ComunityCards
//
//  Created by Bunty on 23/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import Stripe
import SVProgressHUD
import Alamofire

class StripeViewController: UIViewController,STPPaymentCardTextFieldDelegate {

    let cardField = STPPaymentCardTextField()
    var theme = STPTheme.default()
    @IBOutlet weak var doneButton:UIBarButtonItem!
    @IBOutlet weak var bkgView:UIView!
    var amount : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        bkgView.addSubview(cardField)
        edgesForExtendedLayout = []
        bkgView.backgroundColor = theme.primaryBackgroundColor
        cardField.backgroundColor = theme.secondaryBackgroundColor
        cardField.textColor = theme.primaryForegroundColor
        cardField.placeholderColor = theme.secondaryForegroundColor
        cardField.borderColor = theme.accentColor
        cardField.borderWidth = 1.0
        cardField.textErrorColor = theme.errorColor
        cardField.postalCodeEntryEnabled = true
        cardField.delegate = self
        self.setDefaultValues()
        self.setStatusBarColor()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cardField.becomeFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let padding: CGFloat = 20
        cardField.frame = CGRect(x: padding,
                                 y: padding,
                                 width: view.bounds.width - (padding * 2),
                                 height: 50)
    }
    
    //Set Status Bar Color
    func setStatusBarColor() {
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.DefaultAppColor)
            view.addSubview(statusbarView)
          
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
          
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.DefaultAppColor)
        }
    }
    
    // Set Card Default Values
    func setDefaultValues() {
        
        let cardParams = STPPaymentMethodCardParams()
        // Only successful 3D Secure transactions on this test card will succeed.
        cardParams.number = "4242 4242 4242 4242"
        cardParams.expMonth = 12
        cardParams.expYear = 22
        cardParams.cvc = "123"
        cardField.cardParams = cardParams
    }
    
    // MARK: STPPaymentCardTextFieldDelegate
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        // Set button enable state
        //self.doneButton.isEnabled = textField.isValid
    }
    
    @IBAction func doneAction(sender:UIBarButtonItem) {
        
        let status:Bool = self.checkForCardFieldsValidations()
        
        if status == true {
            
            self.getTokenByCardDetails()
        }
    
    }
    
    func getTokenByCardDetails() {
        
        let cardParams = STPCardParams()
        cardParams.number = cardField.cardNumber
        cardParams.expMonth = cardField.expirationMonth
        cardParams.expYear = cardField.expirationYear
        cardParams.cvc = cardField.cvc
        
        SVProgressHUD.show()

        STPAPIClient.shared().createToken(withCard: cardParams) { token, error in
            guard let token = token else {
                
                SVProgressHUD.dismiss()
                
                // Handle the error
                Utility.showAlertMessage(title: "Alert!", message: "\(error.debugDescription)")
                return
            }
            // Use the token in the next step
            if Constants.DEBUG.print { print("Token is \(token)")}
            self.completeCharge(token: token)
        }
        
    }
    
    func completeCharge(token: STPToken) {
      
        CommunityAPI.createChargeFromCards(amount: amount, token: token.tokenId) { (response, error) in
            SVProgressHUD.dismiss()
            if error.count == 0 {
                 if Constants.DEBUG.print { print("Charge is successful") }
                self.postTokenBack(token: token,status:true)
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                    self.postTokenBack(token: token,status:false)
                }
            }
        }
        
   }
    
    func postTokenBack(token:STPToken,status:Bool) {
        
        var tokenDict:Dictionary<String,Any> = [:]
        
        if status == true {
            tokenDict["Token"] = token
        }
        // Post notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetPaymentToken"), object: nil,userInfo:tokenDict)
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: Check For Fields Validations
    
    func checkForCardFieldsValidations() -> Bool {

        if (self.cardField.cardNumber?.count)! < 1 {
            return false
        }
        if self.cardField.expirationMonth < 1 {
            return false
        }
        if self.cardField.expirationYear < 1 {
            return false
        }
        if self.cardField.cvc!.count < 1 {
            return false
        }
        
        return true
    }
    
    @IBAction func cancelAction(sender:UIBarButtonItem) {

        self.dismiss(animated: true, completion: nil)
    }

    
}

extension StripeViewController: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}
