//
//  ContactFormViewController.swift
//  ComunityCards
//
//  Created by Bunty on 21/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD
import DropDown
import CoreLocation

class ContactFormViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var txtFieldSubject:UITextField!
    @IBOutlet weak var lblSubCategory:UILabel!
    @IBOutlet weak var textViewDescription:UITextView!
    @IBOutlet weak var placeHolderLabel:UILabel!
    @IBOutlet weak var categoryButton: UIButton!
    
    var locationManager: CLLocationManager!
    var currentLat : CLLocationDegrees = 0.0
    var currentLong : CLLocationDegrees = 0.0
    
    let chooseCategoryDropDown = DropDown()
    var contactCategories: [ContactCategories] = []
    var categoryNameArray: [String] = []
    var contactCategory : ContactCategories!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureLocationManager()
        self.lblSubCategory.text = "Select Category"
        self.txtFieldSubject.setLeftIcon(UIImage(named: "emailIcon") ?? UIImage())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DropDown.setupDefaultAppearance()
        self.getContactCategories()
        self.setUpViews()
    }
    
    //Set Views
    func setUpViews() {
        
        self.textViewDescription.text = ""
        self.placeHolderLabel.isHidden = false
        self.txtFieldSubject.text = ""
        self.lblSubCategory.text = "Select Category"
    }
    
    //MARK: Configure Location Manager
    
    func configureLocationManager() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 50
        
        // Check for Location Services
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // User location
        if locations.last != nil {
            let location = locations.last
            self.currentLat = (location?.coordinate.latitude)!
            self.currentLong = (location?.coordinate.longitude)!
            locationManager.stopUpdatingLocation()
        }
    }
    
    // MARK: - Setup Choose Category DropDown

    func setupChooseCategoryDropDown() {
        
        self.chooseCategoryDropDown.direction = .bottom
        self.chooseCategoryDropDown.anchorView = self.categoryButton
    
        DispatchQueue.main.async {
            self.chooseCategoryDropDown.bottomOffset = CGPoint(x: 0, y: self.categoryButton.bounds.height)
        }
        // Will set a custom width instead of the anchor view width
        self.chooseCategoryDropDown.width = 270
        // You can also use localizationKeysDataSource instead.
        self.chooseCategoryDropDown.dataSource = self.categoryNameArray
        // Action triggered on selection
        self.chooseCategoryDropDown.selectionAction = { [weak self] (index, item) in
            self?.lblSubCategory.text = item
            self!.contactCategory = self!.contactCategories[index]
        }
    }
    
    // GetContactCategories
    func getContactCategories() {
    
        SVProgressHUD.show()
         CommunityAPI.getContactCategories { (contactCatInfo,error) in
             SVProgressHUD.dismiss()
             if error.count == 0 {
                 if Constants.DEBUG.print { print("Contact Category Array is \(contactCatInfo!)") }
                self.filterArrayBasedOnLanguageStr(contactCategories: contactCatInfo!)
                self.getCategoryNameInfoArray()
             }else {
                 DispatchQueue.main.async {
                     Utility.showAlertMessage(title: "Alert!", message: error)
                 }
             }
         }
        
    }
    
    // Filter Array Based on Languate String
    
    func filterArrayBasedOnLanguageStr (contactCategories:[ContactCategories]) {
        
        if self.contactCategories.count > 0 {
            self.contactCategories.removeAll()
        }
        
        for category in contactCategories {
            
            if let catLanguage = category.categoryLanguage {
                if catLanguage == "EN" {
                    self.contactCategories.append(category)
                }
            }
            
        }
        
    }
    
    // Get CategoryName Info
    func getCategoryNameInfoArray () {
        
        if self.categoryNameArray.count > 0 {
            self.categoryNameArray.removeAll()
        }
        
        for contactCatInfo in self.contactCategories {
            
            if let categoryName = contactCatInfo.categoryName {
                self.categoryNameArray.append(categoryName)
            }
        }
        self.setupChooseCategoryDropDown()
    }
    
    //MARK: Action Methods
    
    // Category View Drop Down
    @IBAction func categoryViewdropDown(_ sender:Any) {
        
        self.chooseCategoryDropDown.show()
    }
    
    // Send Action Method
    @IBAction func sendContactFormAction(_ sender:Any) {
        
        let status:Bool = self.checkForContactFormValidations()
        if status == true {
            self.view.endEditing(true)
            let postData = self.getContactUsFormParameters()
            SVProgressHUD.show()
            CommunityAPI.insertContactUsForm(postData: postData) { (response, error) in
                SVProgressHUD.dismiss()
                if response.count > 0 {
                    if Constants.DEBUG.print { print("Response is \(response)") }
                    DispatchQueue.main.async {
                        Utility.showAlertMessage(title: "Message!", message: "Your query submitted")
                        self.tabBarController?.selectedIndex = 0
                    }
                }else {
                    DispatchQueue.main.async {
                        Utility.showAlertMessage(title: "Alert!", message: error)
                    }
                }
            }
            
        }
    }
    
    //Get Send ContactUs Form Data Parameters
    func getContactUsFormParameters() -> Data {
        
        var userID = ""
        if let userId = UserDefaults.standard.value(forKey: Constants.STRINGS.USERID) {
            userID = userId as! String
        }
        let parameters = "userid=\(userID)&categoryid=\(self.contactCategory.categoryId!)&subject=\(self.txtFieldSubject.text!)&message=\(self.textViewDescription.text!)&lat=\(self.currentLat)&long=\(self.currentLong)"
        let postData =  parameters.data(using: .utf8)
        
        return postData!
    }
    
    //MARK: Check For Validations
    
    func checkForContactFormValidations() -> Bool {
        
        if (self.txtFieldSubject.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter subject")
            return false
        }
        if self.lblSubCategory.text == "Select Category"{
            Utility.showAlertMessage(title: "", message: "Please select category")
            return false
        }
        if (self.textViewDescription.text?.count)! < 1 {
            Utility.showAlertMessage(title: "", message: "Please enter description")
            return false
        }
        return true
    }

    
}

//MARK: Text field Extention

extension ContactFormViewController:  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
  }
    
}

//MARK: TextView Delegate methods

extension ContactFormViewController: UITextViewDelegate {
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        self.placeHolderLabel.isHidden = false
        return true
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        
        let textViewText = (textView.text as NSString)
        if textViewText.length<1 {
            self.placeHolderLabel.isHidden = false
            
        }else {
            self.placeHolderLabel.isHidden = true
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        
        let textViewText = (textView.text as NSString)
        if textViewText.length<1 {
            self.placeHolderLabel.isHidden = false
        }
    }
    
}
