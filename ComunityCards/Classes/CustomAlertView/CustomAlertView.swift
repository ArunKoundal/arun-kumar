//
//  CustomAlertView.swift
//  CustomAlertView
//


import UIKit

class CustomAlertView: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var alertTextField: UITextField!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var textInputView: UIView!
    @IBOutlet weak var bottomCancelButton: UIButton!

    var alertType = Int()
    var cardNumber : Int = 0
    var collectionInfo : CommunityCollection!

    var delegate: CustomAlertViewDelegate?
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
//        cancelButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
//        cancelButton.addBorder(side: .Right, color: alertViewGrayColor, width: 1)
//        actionButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
    }
    
    func setupView() {
//        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.setAlertViewForAlertTypes()
    }
    
    //SetUpAlertForAlertTypes
    func setAlertViewForAlertTypes () {
        
        self.bottomCancelButton.isHidden = true
        self.bottomCancelButton.translatesAutoresizingMaskIntoConstraints = true
        self.actionButton.translatesAutoresizingMaskIntoConstraints = true
        
        self.actionButton.frame = CGRect(x:0.0,y:195.0,width:self.view.frame.size.width-54,height:45.0)
        if self.alertType == AlertType.Insert.rawValue {
            self.textInputView.isHidden = false
            self.cancelView.isHidden = false
            self.circleView.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.DefaultAppColor)
            self.actionButton.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.DefaultAppColor)
            self.actionButton.setTitle("INSERT", for: .normal)
            self.titleLabel.text = "Please insert card number separated by coma";
            self.imageView.image = UIImage.init(named: "card")
        }else if self.alertType == AlertType.Create.rawValue {
            self.textInputView.isHidden = true
            self.cancelView.isHidden = false
            self.circleView.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.CreateAlertColor)
            self.actionButton.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.CreateAlertColor)
            self.actionButton.setTitle("CREATE ALERT", for: .normal)
            self.titleLabel.text = "Do you want to create the alert for card \(self.cardNumber) ?";
            self.imageView.image = UIImage.init(named: "AlertTabIcon")
        }else {
            self.bottomCancelButton.isHidden = false
            self.cancelView.isHidden = true
            self.bottomCancelButton.frame = CGRect(x:0.0,y:195.0,width:(self.view.frame.size.width-54)/2,height:45.0)
            self.actionButton.frame = CGRect(x:(self.view.frame.size.width-54)/2,y:195.0,width:(self.view.frame.size.width-54)/2,height:45.0)
            self.textInputView.isHidden = true
            self.circleView.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.RemoveAlertColor)
            self.actionButton.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.DefaultAppColor)
            self.actionButton.setTitle("CONFIRM", for: .normal)
            self.titleLabel.text = "Do you want to cancel the alert for card \(self.cardNumber) ?";
            self.imageView.image = UIImage.init(named: "closewhite")
        }
        
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    //MARK: Action Methods
    
    @IBAction func onTapCancelButton(_ sender: Any) {
        alertTextField.resignFirstResponder()
        delegate?.cancelButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapBottomCancelButton(_ sender: Any) {
        alertTextField.resignFirstResponder()
        delegate?.cancelButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onTapActionButton(_ sender: Any) {
        
        alertTextField.resignFirstResponder()
        
        if self.alertType == AlertType.Insert.rawValue {
            
            delegate?.actionButtonTapped(textFieldValue: alertTextField.text!)
            
        }else if self.alertType == AlertType.Create.rawValue {

            let createAlert = self.storyboard?.instantiateViewController(withIdentifier: "CreateAlertController") as! CreateAlertController
            createAlert.providesPresentationContextTransitionStyle = true
            createAlert.definesPresentationContext = true
            createAlert.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            createAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            createAlert.cardNumber = self.cardNumber
            createAlert.collectionInfo = self.collectionInfo
            self.present(createAlert, animated: true, completion: nil)
        }else {
            delegate?.actionButtonTapped(textFieldValue: "")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
