//
//  CustomAlertViewDelegate.swift
//  CustomAlertView
//


protocol CustomAlertViewDelegate: class {
    func actionButtonTapped(textFieldValue: String)
    func cancelButtonTapped()
}
