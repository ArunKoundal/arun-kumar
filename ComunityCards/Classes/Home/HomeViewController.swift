//
//  HomeViewController.swift
//  ComunityCards
//
//  Created by Arun Kumar on 27/04/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tblViewList: UITableView!
    var collectionsArray : [CommunityCollection] = []
    var countryCode : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = false
        
        self.countryCode = NSLocale.current.regionCode!
        if Constants.DEBUG.print { print("Country Code is \(self.countryCode)") }
        self.setUpViews()
        self.getCollectionByRegion()
    }
    
    //MARK: Set Up VIew
    
    func setUpViews() {
        
        if let userName = UserDefaults.standard.value(forKey: Constants.STRINGS.USERNAME) {
            self.navigationItem.title = "Welcome \(userName)"
        }
    }
    
    //MARK: LogOutUser
    @IBAction func getuserLogOut(sender:Any) {
        
        UserDefaults.standard.set("", forKey: Constants.STRINGS.TOKEN)
        UserDefaults.standard.set("", forKey: Constants.STRINGS.USERNAME)
        UserDefaults.standard.synchronize()
        let loginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navigationController = UINavigationController(rootViewController: loginController)
        navigationController.navigationBar.isHidden = true
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
    }
    
    //MARK: Get Collection By Region
    
    func getCollectionByRegion() {
        
        if Constants.Connectivity.isConnectedToInternet {
            
            var urlStr = BASEURL.URL + BASEURL.API.GETCOLLECTION
            urlStr = urlStr + "\(self.countryCode)"
            var request = URLRequest(url: URL(string: urlStr)!,timeoutInterval: Double.infinity)
            var accessToken = ""
            if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) {
                accessToken = token as! String
                if Constants.DEBUG.print { print("Token is \(accessToken)") }
            }
            request.addValue(accessToken, forHTTPHeaderField: "X-COG-ID")
            request.httpMethod = "GET"
            
            SVProgressHUD.show()
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                
                SVProgressHUD.dismiss()
              guard let data = data else {
                if Constants.DEBUG.print { print(String(describing: error)) }
                return
              }
                do {
                    
                    if let tempArray = try JSONSerialization.jsonObject(with: data, options:[]) as? [[Any]] {
                    self.collectionsArray = CommunityClasses.getCollectionsInfo(collectionInfo: tempArray)
                    print("Collections Info is \(self.collectionsArray)")
                    DispatchQueue.main.async {
                        self.tblViewList.reloadData()
                    }
                  }
                }catch{
                    if Constants.DEBUG.print { print("Error in Serialization")}
                }
            }
            task.resume()
        }else {
            DispatchQueue.main.async {
                Utility.showAlertMessage(title: "Alert!", message: "No Internet Connection Found")
            }
        }

    }

}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.collectionsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.selectionStyle = .none
        cell.configureCollectionCell(collectionInfo: self.collectionsArray[indexPath.row])
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tblViewList.deselectRow(at: indexPath, animated: true)
        let insertCardController = self.storyboard?.instantiateViewController(withIdentifier: "InsertCardViewController") as! InsertCardViewController
        insertCardController.collectionInfoObj = self.collectionsArray[indexPath.row]
        self.navigationController?.pushViewController(insertCardController, animated: true)
        
    }
    
}
