//
//  HomeTableViewCell.swift
//  ComunityCards
//
//  Created by Arun Kumar on 27/04/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SDWebImage

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionName: UILabel!
    @IBOutlet weak var collectionImgView: UIImageView!
    @IBOutlet weak var collectionBkgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0))
        self.collectionBkgView.translatesAutoresizingMaskIntoConstraints = true
        self.collectionBkgView.frame = CGRect(x: 5.0, y: 0.0, width: 110.0, height: 110.0)
        self.collectionBkgView.layer.masksToBounds = true
        self.collectionBkgView.layer.cornerRadius = 57.0
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCollectionCell(collectionInfo:CommunityCollection) {
        
        if let name = collectionInfo.collectionName {
            self.collectionName.text = name
        }
        if let image = collectionInfo.collectionImage {
            self.collectionImgView.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "Reynolds-card"),options: SDWebImageOptions(rawValue: 0), completed:nil)
        }
        
    }

}
