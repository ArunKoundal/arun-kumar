//
//  InsertCardViewController.swift
//  ComunityCards
//
//  Created by Bunty on 04/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD
import CoreLocation

class InsertCardViewController: UIViewController,CLLocationManagerDelegate{

    var cellIdentifier = "cardCell"

    @IBOutlet weak var cardCollectionView: UICollectionView!
    @IBOutlet weak var insertCardButton: UIButton!
    
    var locationManager: CLLocationManager!
    var currentLat : CLLocationDegrees = 0.0
    var currentLong : CLLocationDegrees = 0.0
    var count : Int = 0
    var collectionInfoObj : CommunityCollection!
    
    var infoDict : Dictionary<String,Any> = [:]

//    var cardsInfoArray:[String] = ["1","2","3","4","5","6","7","8","9","10","11","12"]
      var collectionDetailArray:[CollectionDetail] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCollectionCell()
        self.configureLocationManager()
        self.setUpViews()
        self.addOberverCreateAlertPayment()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
    }
    
    
    //MARK: Set Up VIew
    
    func setUpViews() {
        
        if let collectionName = self.collectionInfoObj.collectionName {
            self.navigationItem.title = "\(collectionName)"
        }
    }
    
    //MARK: Add Oberver For Create Alert Payment
    func addOberverCreateAlertPayment() {
        
        // Register to Create Alert
        NotificationCenter.default.addObserver(self, selector: #selector(InsertCardViewController.getCreateAlertPayment), name: NSNotification.Name(rawValue: "CreateAlertPayment"), object: nil)
        
    }
    // Get Stripe Token
    @objc func getCreateAlertPayment(withNotification notification: NSNotification) {
        
        if Constants.DEBUG.print { print("Post Method Called") }
        self.tabBarController?.selectedIndex = 2
        UserDefaults.standard.set(true, forKey: "CREATEALERT")
        UserDefaults.standard.synchronize()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: Configure Location Manager
    
    func configureLocationManager() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 50
        
        // Check for Location Services
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    // MARK - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // User location
        if locations.last != nil {
            let location = locations.last
            self.currentLat = (location?.coordinate.latitude)!
            self.currentLong = (location?.coordinate.longitude)!
            locationManager.stopUpdatingLocation()
            if count == 0 {
                count += 1
                self.getCollectionDetail()
            }
        }
    }
    
    //MARK: Setup Collection Cell
    
    func registerCollectionCell() {
        
        let cellNib = UINib.init(nibName: "CardCollectionCell", bundle: .main)
        self.cardCollectionView.register(cellNib, forCellWithReuseIdentifier: cellIdentifier)
    }
    
    //MARK: Get Collection Detail
    
    func getCollectionDetail() {
        
        self.infoDict = self.getCollectionDetailParametersInfo()
        SVProgressHUD.show()
        CommunityAPI.getCollectionDetail(data: infoDict) { (response, error) in
            SVProgressHUD.dismiss()
            
            if error == "" {
                if Constants.DEBUG.print { print("Value is \(response!)") }
                self.collectionDetailArray = response!
                DispatchQueue.main.async {
                    self.cardCollectionView.reloadData()
                }
            }else {
                if Constants.DEBUG.print { print("Error is \(error)") }
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: "\(error)")
                }
            }
        }
        
    }
    
    //MARK: Get Collection Detail Parameters Info
    
    func getCollectionDetailParametersInfo() -> Dictionary<String,Any> {
        
        var parametersDict : Dictionary<String,Any> = [:]
        parametersDict["lat"] = self.currentLat
        parametersDict["long"] = self.currentLong
        parametersDict["collectionid"] = self.collectionInfoObj.collectionId
        parametersDict["userid"] = UserDefaults.standard.value(forKey: Constants.STRINGS.USERID)

        if Constants.DEBUG.print { print("Parameters info is \(parametersDict)") }

        return parametersDict
        
    }
    
    //MARK: Insert Card Action
    
    @IBAction func insertCardAction(_ sender:UIButton) {
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertID") as! CustomAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.alertType = AlertType.Insert.rawValue
        self.present(customAlert, animated: true, completion: nil)

    }
    
    // Insert Card
    func insertCardCall(cardsString:String) {
        
        let infoData = self.getInsertCardParameters(cardsInfo: cardsString)
        
        SVProgressHUD.show()
        CommunityAPI.insertCards(postData: infoData) { (response, error) in
            SVProgressHUD.dismiss()
            if response.count > 0 {
                if Constants.DEBUG.print { print("Response is \(response)") }
                self.getCollectionDetail()
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: "\(error)")
                }
            }
        }
        
    }
    
    //MARK: Back Action Method
    
    @IBAction func backActionMethod(sender:UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Get Insert Card parameters
    
    func getInsertCardParameters(cardsInfo:String) -> Data {
        
        var userID = ""
        if let userId = UserDefaults.standard.value(forKey: Constants.STRINGS.USERID) {
            userID = userId as! String
        }
        let parameters = "userid=\(userID)&cards=\(cardsInfo)&collectionid=\(self.collectionInfoObj.collectionId!)"
        let postData =  parameters.data(using: .utf8)
        
        return postData!
    }


}

//MARK: CollectionViewDelegate/Datasource

extension InsertCardViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.collectionDetailArray.count
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
         let cell = self.cardCollectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? CardCollectionCell
        cell?.configureCardCollectionCell(collectionInfo: self.collectionDetailArray[indexPath.row], rowIndex: indexPath.row)

         return cell!
         
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         
         let width = UIScreen.main.bounds.size.width
         let itemWidth = width/3
         return CGSize(width:itemWidth, height:itemWidth)
     }
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         
         self.cardCollectionView.deselectItem(at: indexPath, animated: true)
        let collectionDetail = self.collectionDetailArray[indexPath.row]
        if collectionDetail.nearAreaCardCount! > 0 {
            
            let cardDetailController = self.storyboard?.instantiateViewController(withIdentifier: "CardDetailViewController") as! CardDetailViewController
            cardDetailController.collectionDetail = collectionDetail
            cardDetailController.dictInfo = self.infoDict
            cardDetailController.collectionName = self.collectionInfoObj.collectionName!
            self.navigationController?.pushViewController(cardDetailController, animated: true)

        }else {
            let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertID") as! CustomAlertView
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            customAlert.delegate = self
            customAlert.alertType = AlertType.Create.rawValue
            customAlert.cardNumber = collectionDetail.cardNumber!
            customAlert.collectionInfo = self.collectionInfoObj
            self.present(customAlert, animated: true, completion: nil)
        }

     }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
        
    }
    
}

//MARK: Custom AlertView Delegate

extension InsertCardViewController: CustomAlertViewDelegate {
    
    func actionButtonTapped(textFieldValue: String) {
                    
        if Constants.DEBUG.print { print("TextField has value: \(textFieldValue)") }
        self.insertCardCall(cardsString: textFieldValue)

    }
    
    func cancelButtonTapped() {
        if Constants.DEBUG.print { print("cancelButtonTapped") }
    }
}
