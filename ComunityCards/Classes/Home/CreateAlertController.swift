//
//  CreateAlertController.swift
//  ComunityCards
//
//  Created by Bunty on 17/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD

class CreateAlertController: UIViewController {
    
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var btnAlert: UIButton!
    @IBOutlet weak var navItem: UINavigationItem!

    var collectionInfo : CommunityCollection!
    var cardNumber : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
        self.setStatusBarColor()
    }
    
    //Set up Views
    func setupViews() {
      
        self.lblCardNumber.text = "\(self.cardNumber)"
        if let collectionName = self.collectionInfo.collectionName {
            self.navigationItem.title = collectionName
        }
    }
    
    //Set Status Bar Color
    func setStatusBarColor() {
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.DefaultAppColor)
            view.addSubview(statusbarView)
          
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
          
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.DefaultAppColor)
        }
    }
    
    // Back Action
    @IBAction func backAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    // Alert Action
    @IBAction func alertAction(_ sender: Any) {
        
        self.createAlert()
    }
    
    // Create Alert
    func createAlert() {
        
        let paramsData = self.getCreateAlertParameters()
        SVProgressHUD.show()
        CommunityAPI.createAlert(postData: paramsData) { (response, error) in
            
            SVProgressHUD.dismiss()
            
            if response.count > 0 {
                DispatchQueue.main.async {
                    if Constants.DEBUG.print { print("Alert Created.") }
                    // Post notification
                    //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CreateAlertPayment"), object: nil,userInfo:nil)
                    self.dismiss(animated: true, completion: nil)
                }
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: "\(error)")
                }
            }
        }
    }
    
    //MARK: Switch to Payment Screen
    func switchToPaymentScreen() {
        
        let paymentController = self.storyboard?.instantiateViewController(withIdentifier: "ConfigurationViewController") as! ConfigurationViewController
        paymentController.screenType = true
        paymentController.cardNumber = self.cardNumber
//        self.navigationController?.pushViewController(paymentController, animated: true)
        self.present(paymentController, animated: true, completion: nil)
    }
    
    //MARK: Get Create Alert parameters
    
    func getCreateAlertParameters() -> Data {
        
        var userID = ""
        if let userId = UserDefaults.standard.value(forKey: Constants.STRINGS.USERID) {
            userID = userId as! String
        }
        let parameters = "userid=\(userID)&cardnumber=\(cardNumber)&collectionid=\(self.collectionInfo.collectionId!)"
        let postData =  parameters.data(using: .utf8)
        
        return postData!
    }
    
}
