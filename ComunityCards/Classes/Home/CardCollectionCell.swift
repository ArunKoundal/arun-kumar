//
//  CardCollectionCell.swift
//  ComunityCards
//
//  Created by Bunty on 04/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class CardCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var cardNumLabel: UILabel!
    @IBOutlet weak var alertImgView: UIImageView!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: Configure Collection Cell
    
    func configureCardCollectionCell(collectionInfo:CollectionDetail,rowIndex:Int) {
        
        if let number = collectionInfo.cardNumber {
            self.cardNumLabel.text = "\(number)"
        }
        if collectionInfo.nearAreaCardCount! > 0 {
            self.containerView.backgroundColor = UIColor(red: 73/255, green: 160/255, blue: 46/255, alpha: 1)
            self.alertImgView.isHidden = true
            self.cardNumLabel.textColor = UIColor.white
        }else if collectionInfo.peopleCountCard! > 0 {
            self.alertImgView.isHidden = false
            self.cardNumLabel.textColor = UIColor.black
            self.containerView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        }else {
            self.alertImgView.isHidden = false
            self.cardNumLabel.textColor = UIColor.black
            self.containerView.backgroundColor = .white
        }
        
        self.setAlertImgView(collectionInfo: collectionInfo)
    }
    
    func setAlertImgView(collectionInfo:CollectionDetail) {
        
        self.alertImgView.image = UIImage.init(named: "AlertTabIcon")
        if let noOfAlerts = collectionInfo.noOfAlerts {
            if noOfAlerts >= 1 {
                self.alertImgView.image = UIImage.init(named: "bell")
            }
        }

    }

}
