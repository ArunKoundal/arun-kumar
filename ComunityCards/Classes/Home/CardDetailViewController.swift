//
//  CardDetailViewController.swift
//  ComunityCards
//
//  Created by Bunty on 04/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD

class CardDetailViewController: UIViewController {
    
    var cellIdentifier = "cardDetailCell"

    @IBOutlet weak var cardDetailCollView: UICollectionView!
    @IBOutlet weak var cardNumberButton: UIButton!
    var collectionDetail : CollectionDetail!
    var collectionName : String = ""
    var dictInfo : Dictionary<String,Any> = [:]
    var appDelegate = AppDelegate()
    
    //var cardsDetailsArray:[String] = ["02","13","01","10","12","13","02","13"]
    var cardsDetailsArray:[CardDetail] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.setupViews()
        self.registerCardDetailCollectionCell()
        self.getCardDetailInfo()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = false
        self.appDelegate.screenStatus = true

    }
    
    //MARK: Set up Views
    
    func setupViews() {
        self.cardNumberButton.setTitle("\(self.collectionDetail.cardNumber!)", for: .normal)
        self.navigationItem.title = "\(collectionName)"
    }
    
    //MARK: Setup Collection Cell
    
    func registerCardDetailCollectionCell() {
        
        let cellNib = UINib.init(nibName: "CardDetailCollCell", bundle: .main)
        self.cardDetailCollView.register(cellNib, forCellWithReuseIdentifier: cellIdentifier)
    }
    
    //MARK: Get Card Detail Info
    func getCardDetailInfo() {
        self.dictInfo = self.getCardDetailParametersInfo()
        SVProgressHUD.show()
        CommunityAPI.getCardDetail(data: self.dictInfo) { (carDetail, error) in
            SVProgressHUD.dismiss()
            if error == "" {
                if Constants.DEBUG.print { print("Value is \(carDetail!)") }
                self.cardsDetailsArray = carDetail!
                DispatchQueue.main.async {
                    self.cardDetailCollView.reloadData()
                }
            }else {
                if Constants.DEBUG.print { print("Error is \(error)") }
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: "\(error)")
                }
            }
            
        }
        
    }
    
    //MARK: Get Card Detail Parameters Info
    
    func getCardDetailParametersInfo() -> Dictionary<String,Any> {
        
        var parametersDict : Dictionary<String,Any> = [:]
        parametersDict["lat"] = self.dictInfo["lat"] as! Double
        parametersDict["long"] = self.dictInfo["long"] as! Double
        parametersDict["cardnumber"] = self.collectionDetail.cardNumber!
        
        if Constants.DEBUG.print { print("Parameters info is \(parametersDict)") }

        return parametersDict
        
    }
    
    //MARK: Back Action Method
    
    @IBAction func backActionMethod(sender:UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


//MARK: CollectionViewDelegate/Datasource

extension CardDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.cardsDetailsArray.count
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
         let cell = self.cardDetailCollView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? CardDetailCollCell
        cell?.configureCardDetailCollCell(cardDetail: self.cardsDetailsArray[indexPath.row])

         return cell!
         
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         
         let itemWidth = UIScreen.main.bounds.size.width
        return CGSize(width:itemWidth, height:50.0)
     }
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         
         self.cardDetailCollView.deselectItem(at: indexPath, animated: true)
        let chatViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatViewController.screenStatus = true
        chatViewController.cardNumber = self.collectionDetail.cardNumber!
        let cardDetail = self.cardsDetailsArray[indexPath.row]
        if let userName = cardDetail.userName {
            chatViewController.receiverUserName = userName
        }
        self.navigationController?.pushViewController(chatViewController, animated: true)

     }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
        
    }
    
}
