//
//  CardDetailCollCell.swift
//  ComunityCards
//
//  Created by Bunty on 04/05/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class CardDetailCollCell: UICollectionViewCell {

    @IBOutlet weak var userCardLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceImgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: Configure Card Detail Collection Cell
    
    func configureCardDetailCollCell(cardDetail:CardDetail) {
        
        if let userName = cardDetail.userName {
            self.userCardLabel.text = userName
        }
        self.distanceLabel.text = ""
        if let distance = cardDetail.distanceInStr {
            self.distanceLabel.text = distance
        }
    
    }

}
