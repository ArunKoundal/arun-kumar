//
//  AlertTableViewCell.swift
//  ComunityCards
//
//  Created by Arun Kumar on 27/01/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class AlertTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblCollectionName: UILabel!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var viewCardBkgV : UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //Configre Alert Cell
    func configureAlertCell(alertInfo:AlertDetail) {
        
        if let collectionName = alertInfo.collectionName {
            self.lblCollectionName.text = collectionName
        }
        if let alertDate = alertInfo.formattedCompletedDate {
            self.lblDate.text = alertDate
        }
        if let cardNumber = alertInfo.cardNumber {
            self.lblCardNumber.text = "\(cardNumber)"
        }
        self.lblTime.isHidden = true
        if alertInfo.timeStr != nil {
            self.imgView.image = UIImage.init(named: "tick")
            self.lblCardNumber.textColor = UIColor.white
            self.viewCardBkgV.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.DefaultAppColor)
            self.lblCollectionName.textColor = UIColor.black
            self.containerView.setUpViewBorderColorDefaultGreen()
        }else {
            self.imgView.image = UIImage.init(named: "menu")
            self.lblCardNumber.textColor = UIColor.black
            self.viewCardBkgV.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.COLOR.viewLightGrey)
            self.lblCollectionName.textColor = UIColor.black
            self.containerView.setUpViewBorderColorGrey()
        }
        
    }

}
