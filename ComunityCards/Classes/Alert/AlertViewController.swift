//
//  AlertViewController.swift
//  ComunityCards
//
//  Created by Arun Kumar on 27/01/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD

class AlertViewController: UIViewController {
    
    @IBOutlet weak var tblViewAlert: UITableView!
    
    var alertHistoryArray : [AlertDetail] = []
    var sectionsArray : [String] = []
    var groupsInfoDict : Dictionary<String,Any> = [:]
    var alertInfo : AlertDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.alertHistoryArray.removeAll()
        self.getAlertHistory()
    }
    
    //MARK: Get Alert History
    func getAlertHistory() {
        
        SVProgressHUD.show()
        CommunityAPI.getAlertHistory { (alertHistoryInfo,error) in
            SVProgressHUD.dismiss()
            if error.count == 0 {
                if Constants.DEBUG.print { print("Alert History Array is \(alertHistoryInfo!)") }
                self.getGroupingOfArrayBasedOnDates(infoArray:alertHistoryInfo!)
                DispatchQueue.main.async {
                    self.tblViewAlert.reloadData()
                }
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                }
            }
        }
        
    }
    
    // Get Grouping of alerts
    func getGroupingOfArrayBasedOnDates(infoArray:[AlertDetail]) {
        
        alertHistoryArray = infoArray.sorted(by: {
            $0.date.compare($1.date) == .orderedDescending
        })
        self.sectionsArray.removeAll()
        var tempDateStr : String = ""
        var tempAlertsArray : [AlertDetail] = []
        for infoObj in self.alertHistoryArray {
            
            let dateString = self.getDateString(infoObj: infoObj)

            if tempDateStr == dateString {
                tempAlertsArray.append(infoObj)
                self.groupsInfoDict[dateString] = tempAlertsArray
            }else {
                tempAlertsArray.removeAll()
                tempAlertsArray.append(infoObj)
                self.groupsInfoDict[dateString] = tempAlertsArray
                tempDateStr = dateString
                self.sectionsArray.append(tempDateStr)
            }
        }
        if Constants.DEBUG.print { print("Section array is \(self.sectionsArray )") }
        if Constants.DEBUG.print { print("Group info dict is \(self.groupsInfoDict)") }
    }
    
    //GetDateString
    func getDateString(infoObj:AlertDetail) -> String {
        
        var dateString = ""
        let todayDateStr = NSDate.getDateStringFromDate(format: Constants.dateFormat.DATE_FORMAT_UI, date: Date())
        let tomorrow = Date().addingTimeInterval(-86400)
        let tomorrowDateStr = NSDate.getDateStringFromDate(format: Constants.dateFormat.DATE_FORMAT_UI, date: tomorrow)

        if todayDateStr == infoObj.formattedCompletedDate {
            dateString = "Today"
        }else if tomorrowDateStr == infoObj.formattedCompletedDate {
            dateString = "Tomorrow"
        }else {
            dateString = infoObj.formattedCompletedDate!
        }
        
        return dateString
    }
    
    //MARK: Remove Alert Call
    func removeAlertCall(alertInfo:AlertDetail) {
        
        SVProgressHUD.show()
        CommunityAPI.removeAlertFromHistory(alertId: alertInfo.alertId!) { (response, error) in
            SVProgressHUD.dismiss()
            if error.count == 0 {
                if Constants.DEBUG.print { print("Response is \(response)") }
                self.getAlertHistory()
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: error)
                }
            }
        }

        
    }
    
    func showAlertForRemoveAlert() {
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertID") as! CustomAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.alertType = AlertType.Cancel.rawValue
        customAlert.cardNumber = self.alertInfo.cardNumber!
        self.present(customAlert, animated: true, completion: nil)
    }
    
}

extension AlertViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.sectionsArray.count

    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0//UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let alertsArray = self.groupsInfoDict[self.sectionsArray[section]] as! [AlertDetail]
        return alertsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = UIColor.white
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.font = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        label.textColor = UIColor.black
        headerView.addSubview(label)
        label.text = self.sectionsArray[section]

        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlertTableViewCell") as! AlertTableViewCell
        let alertsArray = self.groupsInfoDict[self.sectionsArray[indexPath.section]] as! [AlertDetail]
        cell.configureAlertCell(alertInfo: alertsArray[indexPath.row])

        return cell
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tblViewAlert.deselectRow(at: indexPath, animated: true)
        let alertsArray = self.groupsInfoDict[self.sectionsArray[indexPath.section]] as! [AlertDetail]
        self.alertInfo = alertsArray[indexPath.row]
        self.showAlertForRemoveAlert()
    }
    
}


//MARK: Custom AlertView Delegate

extension AlertViewController : CustomAlertViewDelegate {
    
    func actionButtonTapped(textFieldValue: String) {

        self.removeAlertCall(alertInfo: self.alertInfo)
    }
    
    func cancelButtonTapped() {
        if Constants.DEBUG.print { print("cancelButtonTapped") }
    }
    
}
