//
//  TutorialViewController.swift
//  ComunityCards
//
//  Created by Arun Kumar on 27/04/20.
//  Copyright © 2020 Cards. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {
    
    @IBOutlet weak var viewPager: ViewPager!
    
    var screenImagesArray : [String] = ["logov3.png","Collections.png","CollectionDetail.png","CardDetail.png","ChatScreen.png","CreateAlert.png"]
    var screenTextArray : [String] = ["WELCOME TO","SELECT ONE OF THE ACTIVE COLLECTIONS","SECLECT THE CARD YOU NEED OR REGISTER YOURS","SEE THE ONES CLOSEST TO YOU","TALK TO OWNER OF THE CARD AND MAKE THE TRADE","IF YOU WANT YOU CAN SET AN ALERT FOR A SPECIFIC CARD"]

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.alpha = 1.0
        self.navigationController?.isNavigationBarHidden = true
        viewPager.dataSource = self;
        viewPager.animationNext()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewPager.scrollToPage(index: 0)
    }
    
    @IBAction func action_registration(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        viewController.screenType = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func action_login(_ sender: Any) {
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") else { return }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func gotoLoginScreen(sender:UIButton) {
        
        guard let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") else { return }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension TutorialViewController:ViewPagerDataSource{
    func numberOfItems(viewPager:ViewPager) -> Int {
        return 6;
    }
    
    func viewAtIndex(viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        var newView = view;
        var label:UILabel?
        var logoImageView:UIImageView?
        var screenImageView:UIImageView?
        var finishBtn:UIButton?
        var titleLabel:UILabel?

        if(newView == nil){
            newView = UIView(frame: CGRect(x: 10, y: 0, width: self.viewPager.frame.width-20, height:  self.viewPager.frame.height))
            newView!.backgroundColor = UIColor(red: 102/255, green: 204/255, blue: 102/255, alpha: 1.0)
            
            label = UILabel.init(frame: CGRect(x:70.0,y:10.0,width:(newView?.frame.size.width)!-140.0,height:130.0))
            label!.tag = 1
            label!.textAlignment = .center
            label!.numberOfLines = 4
            label!.textColor = UIColor.white
            label!.font = UIFont.systemFont(ofSize: 26.0, weight: .thin)
            newView?.addSubview(label!)
            screenImageView = UIImageView.init(frame: CGRect(x:40.0,y:140.0,width:(newView?.frame.size.width)!-80.0,height:(newView?.frame.size.height)! - 170.0))
            screenImageView!.tag = 1
            screenImageView!.contentMode = .scaleAspectFit
            newView?.addSubview(screenImageView!)
            screenImageView?.isHidden = false
            logoImageView?.isHidden = true
            titleLabel?.isHidden = true
            if index == 0 {
                logoImageView = UIImageView.init(frame: CGRect(x:40.0,y:120.0,width:(newView?.frame.size.width)!-80.0,height:(newView?.frame.size.height)! - 285.0))
                logoImageView!.tag = 1
                logoImageView!.contentMode = .scaleAspectFit
                logoImageView?.image = UIImage.init(named: "logov3.png")
                newView?.addSubview(logoImageView!)
                screenImageView?.isHidden = true
                logoImageView?.isHidden = false
                
                titleLabel = UILabel.init(frame: CGRect(x:70.0,y:120.0 + (logoImageView?.frame.size.height)! + 5.0,width:(newView?.frame.size.width)!-140.0,height:130.0))
                titleLabel!.tag = 1
                titleLabel!.textAlignment = .center
                titleLabel!.numberOfLines = 3
                titleLabel!.textColor = UIColor.white
                titleLabel!.font = UIFont.systemFont(ofSize: 26.0, weight: .thin)
                titleLabel?.text = "THE COLLECTIBLE CARDS COMMUNITY APP"
                newView?.addSubview(titleLabel!)
            }
            if index == 5 {
                finishBtn = UIButton.init(frame: CGRect(x:(newView?.frame.size.width)! - 65.0,y:(newView?.frame.size.height)! - 35.0,width:80.0,height:30.0))
                finishBtn?.setTitle("FINISH", for: .normal)
                finishBtn?.titleLabel?.textColor = UIColor.white
                finishBtn?.backgroundColor = UIColor.darkGray
                newView?.addSubview(finishBtn!)
                finishBtn?.isUserInteractionEnabled = true
                finishBtn?.addTarget(self, action: #selector(TutorialViewController.gotoLoginScreen(sender:)), for: .touchUpInside)
            }
        }else{
          label = newView?.viewWithTag(1) as? UILabel
          screenImageView = newView?.viewWithTag(1) as? UIImageView
            if index == 5 {
              finishBtn = newView?.viewWithTag(1) as? UIButton
            }
        }
        label?.text = self.screenTextArray[index]
        screenImageView?.image = UIImage.init(named: self.screenImagesArray[index])
        return newView!
    }

    func didSelectedItem(index: Int) {
        print("select index \(index)")
    }
   
}
