//
//  AppDelegate.swift
//  ComunityCards
//
//  Created by Sumeet on 27/12/19.
//  Copyright © 2019 Cards. All rights reserved.
//

import UIKit
import SVProgressHUD
import DropDown
import Stripe
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var screenStatus : Bool = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        Stripe.setDefaultPublishableKey("pk_test_Z1a6lVQbhK1XpnXvtXwU1tP400aVWD1R1k")
        SVProgressHUD.setDefaultMaskType(.black)
        DropDown.startListeningToKeyboard()
        self.checkTokenValidity()
        //Register For APNS
        self.configureAPNSNotification(application: application)
    
        return true
    }
    
    func checkTokenValidity() {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

        if UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) != nil {
            
            SVProgressHUD.show()
            CommunityAPI.checkTokenValidation { (status, error) in
                SVProgressHUD.dismiss()
                if status == true {
                    DispatchQueue.main.async {
                        let tabBarCont = storyBoard.instantiateViewController(withIdentifier: "AppTabBarController") as! AppTabBarController
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = tabBarCont
                    }
                }else {
                    DispatchQueue.main.async {
                        let loginController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        let navigationController = UINavigationController(rootViewController: loginController)
                        navigationController.navigationBar.isHidden = true
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = navigationController
                    }
                }
            }

        }else {
            DispatchQueue.main.async {
                let tutorialController = storyBoard.instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
                let navigationController = UINavigationController(rootViewController: tutorialController)
                navigationController.navigationBar.isHidden = true
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
            }
        }
    }
    
    //MARK: Configure Push Notification

     func configureAPNSNotification(application:UIApplication) {
         
         if #available(iOS 10.0, *) {
           // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

           let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
           UNUserNotificationCenter.current().requestAuthorization(
             options: authOptions,
             completionHandler: {_, _ in })
         } else {
           let settings: UIUserNotificationSettings =
           UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
           application.registerUserNotificationSettings(settings)
         }

         application.registerForRemoteNotifications()
     }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {

        // Print full message.
        if Constants.DEBUG.print { print(userInfo) }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
                
        // Print full message.
        if Constants.DEBUG.print { print("Message in Background : \(userInfo)") }

        completionHandler(UIBackgroundFetchResult.newData)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        if Constants.DEBUG.print { print("Unable to register for remote notifications: \(error.localizedDescription)") }
       
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        if Constants.DEBUG.print { print("APNs token retrieved: \(deviceToken)")}
        let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        if Constants.DEBUG.print { print("Token in string : \(tokenString)") }
        UserDefaults.standard.set(tokenString, forKey: Constants.STRINGS.DEVICE_TOKEN)
        UserDefaults.standard.synchronize()
        if let token = UserDefaults.standard.value(forKey: Constants.STRINGS.TOKEN) as? String {
            if token.count > 0 {
                self.submitIOSDeviceTokenToServer()
            }
        }
    }
    
    //MARK: Submit Token to Server
    
    func submitIOSDeviceTokenToServer() {
                    
        CommunityAPI.submitDeviceTokenToServer { (response, error) in
            
            if response.count > 0 {
                if Constants.DEBUG.print { print("Token Submitted") }
            }else {
                DispatchQueue.main.async {
                    Utility.showAlertMessage(title: "Alert!", message: "\(error)")
                }
            }
            
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

//MARK: UNUserNotificationCenter Delegates

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    
    let notifInfo = notification.request.content.userInfo
    // Print full message.
    if Constants.DEBUG.print { print("Notification is \(notifInfo)") }
    
    completionHandler([.alert, .badge, .sound])
  }
    

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    
    let notifInfo = response.notification.request.content.userInfo
    // Print full message.
    if Constants.DEBUG.print { print("Notification is \(notifInfo)") }

    completionHandler()
  }

}


